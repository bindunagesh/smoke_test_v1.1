# Hello!

## Cloning the Repository

1. Navigate to the root of the cloned repository using the terminal.
2. Run the command the following command 
``
git clone https://gitlab.com/mainbuildrepo/cypressautomation.git
``
3. This will create a copy of the repository on your local machine.

## Installing Dependencies
1. Navigate to the root of the cloned repository using the terminal.
2. Run the command`` npm install`` to install the project's dependencies.
3. This will install Cypress and any other necessary packages.

## Running The Test
1. To run the tests, navigate to the root of the project in the terminal
2. Run the command ```npx cypress open``` or ```npx cypress open```
 

### Have a good day...




















  
