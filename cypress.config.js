const { defineConfig } = require("cypress");
const path = require("path");
const gmail = require("gmail-tester");
const sqlServer = require("cypress-sql-server");
const { registerAIOTestsPlugin } = require('cypress-aiotests-reporter/src')
const fs = require('fs');
const cypress = require("cypress");

let specFileName;


////Dont delete any commented code/////
// const fs = require('fs-extra')
// function getConfigurationByFile(file) {
//   const pathToConfigFile = path.resolve(
//     '..',
//     'Cypress-env-configs/cypress/config-files',
//     `${file}.json`
//   );

//   return fs.readJson(pathToConfigFile);
// }

module.exports = defineConfig({
  projectId: 'iahqbg',
  //repoter: 'cypress-mochawesome-reporter', //for generating html report
  reporter: "mochawesome",
  reporterOptions: {
      reportDir: "cypress/reports/mochawesome-report",
      reportFilename: "[status]_[datetime]-[name]-report",
      timestamp: "longDate",
      overwrite: false,
      html: true,
      json: true  
   },
   screenshotsFolder: "cypress/reports/mochawesome-report/assets/[status]_[datetime]-[name]-screenshot",

   "log": true,
  "chromeWebSecurity": false,



  
  env: {
    //"sso_url": "http://login.microsoftonline.com/535ab425-5c4d-4b58-9a3b-f1906e8e40b2/saml2?SAMLRequest=rVJNb5wwEP0rlu8GbGBhrWWjbaOokVJlFUgPvRkzEFdgbz1mVfXXl%2ByHur3k1NvY896b53ne3P2aRnIEj8bZivIooQSsdp2xQ0VfmwdW0rvtBtU0ioPczeHNvsDPGTCQhWhRnjsVnb2VTqFBadUEKIOW9e7rkxRRIg%2FeBafdSMkOEXxYRn12FucJfA3%2BaDS8vjxV9C2EA8o4%2Fg2WoT%2FyIloqdwhGY6TdJMssS%2BOluLnGMx3jdxtxXT9Tcr94M1aF03uukqMbjI0mo71D1wdnR2PhXTTO01y1mchZrrOOZW1esrVKW9bzdbKCErKkFSd1QcmD8xpOO6hor0YESh7vK6pEMfRQrn6A6rKC51yUSTes%2BiEtdcq7BYR7hWiO8JeGOMOjxaBsqKhIRMo4Z0I0SSHTteRlxFfFd0r2l819MvacyEdrbs8glF%2BaZs%2F2z3VDybdrsguAXnKUp%2Bn%2BNsCPhdU1Nbr9Hxlt4lsb28vx39%2B1%2FQM%3D&SigAlg=http%3A%2F%2Fwww.w3.org%2F2000%2F09%2Fxmldsig%23dsa-sha1&Signature=MC0CFFPKxQQ%2FwTFRV2xFidviiDjJqUf%2BAhUAlDGWn1ThXhdjCTqgn6jx9uACU24%3D",
    "url": "https://46.17.102.28:8443/com.zenoptics.services/login.jsp",
    //"qa2": "https://qa2.zenoptics.com:8443/com.zenoptics.services/login.jsp",
    //"trial": "https://trial.zenoptics.com:8443/com.zenoptics.services/login.jsp",
    //"winTrial": "https://win-trial.zenoptics.com:8443/com.zenoptics.services/login.jsp",
    //"srv14": "https://zen-srv14.zenoptics.com:8443/com.zenoptics.services/login.jsp",
    "adminUserName":"hruser",
    "adminPassword":"password1234",
    "username1": "User123",
    "password1": "User@123",
    "reg_username": "yash",
    "reg_password": "11111",

  
    "aioTests": {
      "enableReporting": true,
      "hosted" : {
        "jiraUrl": "https://zenoptics.atlassian.net",
        "jiraPAT": "Mjg5OTAzOTgtZDNmMS0zNTJjLThkMjgtYWZhZGRkYjIyMGQzLjcwMzk1NTg1LWEwNTYtNDMzYS05MWE4LWYyMTJlZTU4YzExOA==",
        "jiraUsername": "bindu@zenoptics.com",
        "jiraPassword": "User@123"
      },
      //"cloud": { 
        //"apiKey": "NzU2ZjZiZmQtMGU4MS00YjlmLWI3ZTgtOWM1MTkxODA3MGVj"
        
      //},
      "jiraProjectId": "ZO",
      "cycleDetails": {
        "createNewCycle": false,
        //"cycleName": "Cypress Automated SaintySuite Run",
        //"cycleKey": "ZEN2022-CY-16",
        "cycleName": "Ad hoc",
        "cycleKey": "ZEN2022-CY-Adhoc"
      },
      "addNewRun": true,
      "addAttachmentToFailedCases": true,
      "createNewRunForRetries": true
    },


  //reporter: "cypress-mochawesome-reporter",
  


},
  e2e: {
    experimentalModifyObstructiveThirdPartyCode: true,
    video: false,
    setupNodeEvents(on, config) {

      // Ensure the logs directory exists
      const logsDir = path.join(config.projectRoot, 'logs');
      if (!fs.existsSync(logsDir)) {
        fs.mkdirSync(logsDir);
      }

      // Format today's date and time
      const now = new Date();
const year = now.getFullYear();
const month = String(now.getMonth() + 1).padStart(2, '0');
const day = String(now.getDate()).padStart(2, '0');
const hour = String(now.getHours()).padStart(2, '0');
const minute = String(now.getMinutes()).padStart(2, '0');
const second = String(now.getSeconds()).padStart(2, '0');

// Create the global log file with date and time
const formattedDate = `${year}-${month}-${day} ${hour}-${minute}-${second}`;

  // Create the global log file with date and time
  const globalLogFileName = `${formattedDate}_global.log`;
  //const globalLogFileName = `[name]_[datetime]-global.log`;
  const globalLogFilePath = path.join(logsDir, globalLogFileName);

  on('task', {
    taskLogToTerminalReport: (message) => {
      //console.log(message);

      // Append the message to the global log file
      fs.appendFileSync(globalLogFilePath, message + '\n');

      // Return null to avoid any interference with other tasks
      return null;
        },
      });
      

//       module.exports = (on, config) => {
//   const file = config.env.fileConfig || 'development';

//   return getConfigurationByFile(file);
// };
      
      
      
       registerAIOTestsPlugin(on,config);
      require("cypress-mochawesome-reporter/plugin")(on);
      // implement node event listeners here
      on("task", {
        "gmail:check": async (args) => {
          const { from, to, subject } = args;
          const email = await gmail.check_inbox(
            path.resolve(__dirname, "credentials.json"), // credentials.json is inside plugins/ directory.
            path.resolve(__dirname, "gmail_token.json"), // gmail_token.json is inside plugins/ directory.
            {
              subject: subject,
              from: from,
              to: to,
              include_body: true,
              wait_time_sec: 10,
              max_wait_time_sec: 30,
            } // Maximum poll interval (in seconds). If reached, return null, indicating the completion of the task().
          );
          return email;
        },
      });

      // allows db data to be accessed in tests
      config.db = {
        userName: "sa",
        password: "ZenOptics1234!",
        server: "54.177.76.22",
        options: {
          database: "zenoptics",
          encrypt: true,
          rowCollectionOnRequestCompletion: true,
        },
      };

      // code from /plugins/index.js
      const tasks = sqlServer.loadDBPlugin(config.db);
      on("task", tasks);

      return config;



    },
    
  },

  defaultCommandTimeout: 100000,
  retries: {
    // Configure retry attempts for `cypress run`
    // Default is 0
    runMode: 0,
    // Configure retry attempts for `cypress open`
    // Default is 0
    openMode: 0,
  },

  viewportWidth: 1366,
  viewportHeight: 768,
});
