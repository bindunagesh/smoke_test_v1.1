import * as dashboardLocators from "../locators/dashboard_locators";
import * as gobalLocaters from "../locators/gobal_locators";
import dashboardLinks from "../fixtures/dashboard_links.json"
import * as dashboard from "../Pages/dashboard_PO"
// import * as report from '../Pages/report_PO'
import * as report from '../Pages/report_PO'
import data from "../fixtures/sanityData.json"
import { logToGlobalFile } from '../utilities/global_log'


export function gobalSearch(report) {
  logToGlobalFile("Wait: 2000 milliseconds")
  cy.wait(2000)
  logToGlobalFile(` Clicking on the global search input field, Clearing the existing text, Typing "${report}" into the global search input field`)
  return cy.get(gobalLocaters.gobalSearch.search).click().clear().type(report);

}
export function gobalSearch_(report) {
  logToGlobalFile("Wait: 2000 milliseconds")
  cy.wait(2000)
  logToGlobalFile(` Clicking on the global search input field, Clearing the existing text, Typing "${report}" into the global search input field`)
  cy.get(gobalLocaters.gobalSearch.search).click().clear().type(report);
  cy.get("div[class='zo-list-data ng-star-inserted']").eq(0).click()
  
}

//added
export function advanceSearch(report) {
  cy.wait(2000)
  cy.get(gobalLocaters.gobalSearch.search).click().clear().type(report);
  cy.get(gobalLocaters.gobalSearch.advanceSearch).click()
  //cy.go('back')
  //cy.reload()
}

export function gobalSearchResult(report) {
  return cy
    .get(gobalLocaters.gobalSearch.searchCard).eq(0)
    .click()
    .contains(report).click();
}
//modified
export function searchAndSelectReport(report) {
  logToGlobalFile('Wait: 2000 milliseconds before searching')
  cy.wait(2000)//added
  logToGlobalFile(`Search and select report: ${report}`)
  gobalSearch(report);
  gobalSearchResult(report).click();
  logToGlobalFile('Wait: 2000 milliseconds after searching')
  cy.wait(2000)
  logToGlobalFile(`Click on search card for report: ${report}`)
  cy.get(gobalLocaters.gobalSearch.searchCard).eq(0).contains(report).click();
}

export function searchAndSelectFirstReport(report) {
  
  gobalSearch(report);
  logToGlobalFile(`Click on searched first report: ${report}`)
  return cy.get(gobalLocaters.gobalSearch.searchFirstReport).click();
}

export function verifytheViewQuickSearch()
{
  cy.get(gobalLocaters.quick_search.view).eq(0).trigger('mouseover').should('be.visible')
  cy.get(gobalLocaters.quick_search.rating).eq(0).trigger('mouseover').should('be.visible')
  cy.get(gobalLocaters.quick_search.info).eq(0).click()
}

export function verifytheViewAdvanceSearch()
{

  cy.get(gobalLocaters.advance_Search.view).eq(0).trigger('mouseover').should('be.visible')
  cy.get(gobalLocaters.quick_search.rating).eq(0).trigger('mouseover').should('be.visible')
  
}

export function verifytheviews(tab){
  dashboard.openMenu() 
  dashboard.clickSideMenuLink("Reports")
  logToGlobalFile(` Clicking on the "${tab}" tab`)
cy.get(dashboardLocators.leftNavMenu.reportMenu).contains(tab).click()
cy.get(gobalLocaters.reportViews.viewSelection)
logToGlobalFile("Clicking on the Thumbnail View")
cy.get(gobalLocaters.reportViews.thumbnailView).click()
logToGlobalFile("Verifying the existence of the Thumbnail Container");
cy.get(gobalLocaters.reportViews.thumbnailContainer).should('exist')
cy.screenshot('pass-test-004_1')
logToGlobalFile("Clicking on the List View"); 
cy.get(gobalLocaters.reportViews.listView).click()
logToGlobalFile("Verifying the existence of the List Container"); 
cy.get(gobalLocaters.reportViews.listContainer).should('exist')
cy.screenshot('pass-test-004_2')
logToGlobalFile("Clicking on the Grid View")
cy.get(gobalLocaters.reportViews.gridView).click()
logToGlobalFile("Verifying the existence of the Grid Container"); 
cy.get(gobalLocaters.reportViews.gridContainer).should('exist')
cy.screenshot('pass-test-004_3')

}

export function viewReportIndifferentViews(ReportName){
  


 cy.get(gobalLocaters.reportViews.viewSelection)
 cy.get(gobalLocaters.reportViews.thumbnailView).click()
 cy.get(gobalLocaters.reportViews.thumbnailContainer).should('exist')
 cy.get(gobalLocaters.reportViews.thumbnailReportName).contains(ReportName).should('exist')
 cy.get(gobalLocaters.reportViews.listView).click()
 cy.get(gobalLocaters.reportViews.listContainer).should('exist')
 cy.get(gobalLocaters.reportViews.listViewReportName).contains(ReportName).should('exist')
 cy.get(gobalLocaters.reportViews.gridView).click()
 cy.get(gobalLocaters.reportViews.gridContainer).should('exist')
 cy.get(gobalLocaters.reportViews.gridReporNAme).contains(ReportName).should('exist')

}

export function viewReportIndifferentViewsCategoriesPage(ReportName){
  


  cy.get(gobalLocaters.reportViews.viewSelection)
  cy.get(gobalLocaters.reportViews.thumbnailView).click()
  cy.get(gobalLocaters.reportViews.thumbnailContainer2).should('exist')
  cy.get(gobalLocaters.reportViews.thumbnailReportName2).contains(ReportName).should('exist')
  cy.get(gobalLocaters.reportViews.listView).click()
  cy.get(gobalLocaters.reportViews.listContainer).should('exist')
  cy.get(gobalLocaters.reportViews.listViewReportName).contains(ReportName).should('exist')
  cy.get(gobalLocaters.reportViews.gridView).click()
  cy.get(gobalLocaters.reportViews.gridContainer).should('exist')
  cy.get(gobalLocaters.reportViews.gridReporNAme).contains(ReportName).should('exist')
 
 }

 




export function verifyMultiView(reportnumber,multiview){
  logToGlobalFile("Clicking on grid view");
  cy.get(gobalLocaters.reportViews.gridView).click()
  logToGlobalFile("Verify Gridview container exist");
 cy.get(gobalLocaters.reportViews.gridContainer).should('exist')
  for (let i = 0; i<=reportnumber; i++){
    logToGlobalFile("Open report from gridview");
    cy.get(gobalLocaters.reportViews.gridreport).eq(i).click()
    logToGlobalFile("Clicking on back");
    cy.go('back')
    }
    cy.get(gobalLocaters.reportViews.gridreport).eq(0).click()
    logToGlobalFile("Clicking on multiview button");
    cy.get(gobalLocaters.reportViews.multiviewbutton).click()
    logToGlobalFile("Clicking on multiview enabled button");
    cy.get(gobalLocaters.reportViews.multiviewenablebutton).click()
    logToGlobalFile("Verifying the existence of the split view");
    cy.get(gobalLocaters.reportViews.splitview).should('exist')
    logToGlobalFile("Verifying the number of reports in split view");
    cy.get(gobalLocaters.reportViews.splitviewreports)
    .should('have.length', multiview)
    logToGlobalFile("Clicking on the single view button");
    
    cy.get(gobalLocaters.reportViews.singleviewbutton).click()
    logToGlobalFile("Verifying the existence of the single view");
    
    cy.get(gobalLocaters.reportViews.singleview).should('exist')
    logToGlobalFile('Verifying the number of reports in single view');
    cy.get(gobalLocaters.reportViews.singleviewreports)
    .should('have.length', multiview)
    
}

export function verifySplitView(reportnumber,multiview){
  cy.get(gobalLocaters.reportViews.gridView).click()
 cy.get(gobalLocaters.reportViews.gridContainer).should('exist')
  for (let i = 0; i<=reportnumber; i++){
    cy.get(gobalLocaters.reportViews.gridreport).eq(i).click()
    cy.go('back')
    }
    cy.get(gobalLocaters.reportViews.gridreport).eq(0).click()
    cy.get(gobalLocaters.reportViews.multiviewbutton).click()
    cy.get(gobalLocaters.reportViews.multiviewenablebutton).click()
    cy.get(gobalLocaters.reportViews.splitview).should('exist')
    cy.get(gobalLocaters.reportViews.splitviewreports)
    .should('have.length', multiview)
  }

  export function verifySingleView(reportnumber,multiview){
    cy.get(gobalLocaters.reportViews.gridView).click()
   cy.get(gobalLocaters.reportViews.gridContainer).should('exist')
    for (let i = 0; i<=reportnumber; i++){
      cy.get(gobalLocaters.reportViews.gridreport).eq(i).click()
      cy.go('back')
      }
      cy.get(gobalLocaters.reportViews.gridreport).eq(0).click()
      cy.get(gobalLocaters.reportViews.multiviewbutton).click()
      cy.get(gobalLocaters.reportViews.singleviewbutton).click()
      cy.get(gobalLocaters.reportViews.multiviewenablebutton).click()
      cy.get(gobalLocaters.reportViews.singleview).should('exist')
      cy.get(gobalLocaters.reportViews.singleviewreports)
      .should('have.length', multiview)
      
  }

  export function verifySingleViewSchedule(reportnumber,multiview){
    cy.get(gobalLocaters.reportViews.gridView).click()
   cy.get(gobalLocaters.reportViews.gridContainer).should('exist')
    for (let i = 0; i<=reportnumber; i++){
      cy.get(gobalLocaters.reportViews.gridreport).eq(i).click()
      cy.go('back')
      }
      cy.get(gobalLocaters.reportViews.gridreport).eq(0).click()
      cy.get(gobalLocaters.reportViews.multiviewbutton).click()
      cy.get(gobalLocaters.reportViews.singleviewbutton).click()
      cy.get(gobalLocaters.reportViews.multiviewenablebutton).click()
      cy.get(gobalLocaters.reportViews.singleview).should('exist')
      cy.get(gobalLocaters.reportViews.singleviewreports)
      .should('have.length', multiview)
      cy.get(gobalLocaters.reportViews.multiviewRefreshSchedule).click()
      cy.get(gobalLocaters.reportViews.multiviewFrequencyBox).click()
      cy.get(gobalLocaters.reportViews.multiviewSelectFrequency).eq(0).click()
      cy.get(gobalLocaters.reportViews.multiViewFrequencyButton).click()
      cy.wait(60000);
      cy.intercept("POST", "**/com.zenoptics.services/jaxrs/AddUsageData**").as("refreshApi");
      cy.wait("@refreshApi", { timeout: 10000 }).then(interception => {
        expect(interception.response.statusCode).to.equal(200);
      });
    cy.get(gobalLocaters.reportViews.toastNotification).should('have.text', ' Refresh initiated ')
      
  }


  export function verifySplitViewSchedule(reportnumber,multiview){
    cy.get(gobalLocaters.reportViews.gridView).click()
   cy.get(gobalLocaters.reportViews.gridContainer).should('exist')
    for (let i = 0; i<=reportnumber; i++){
      cy.get(gobalLocaters.reportViews.gridreport).eq(i).click()
      cy.go('back')
      }
       cy.get(gobalLocaters.reportViews.gridreport).eq(0).click()
    cy.get(gobalLocaters.reportViews.multiviewbutton).click()
    cy.get(gobalLocaters.reportViews.multiviewenablebutton).click()
    cy.get(gobalLocaters.reportViews.splitview).should('exist')
    cy.get(gobalLocaters.reportViews.splitviewreports)
    .should('have.length', multiview)
      cy.get(gobalLocaters.reportViews.multiviewRefreshSchedule).click()
      cy.get(gobalLocaters.reportViews.multiviewFrequencyBox).click()
      cy.get(gobalLocaters.reportViews.multiviewSelectFrequency).eq(0).click()
      cy.get(gobalLocaters.reportViews.multiViewFrequencyButton).click()
      cy.wait(60000);
      cy.intercept("POST", "**/com.zenoptics.services/jaxrs/AddUsageData**").as("refreshApi");
      cy.wait("@refreshApi", { timeout: 10000 }).then(interception => {
        expect(interception.response.statusCode).to.equal(200);
      });
    cy.get(gobalLocaters.reportViews.toastNotification).should('have.text', ' Refresh initiated ')
      
  }

  export function verifyMultiViewSchedule(reportnumber,multiview){
    cy.get(gobalLocaters.reportViews.gridView).click()
   cy.get(gobalLocaters.reportViews.gridContainer).should('exist')
    for (let i = 0; i<=reportnumber; i++){
      cy.get(gobalLocaters.reportViews.gridreport).eq(i).click()
      cy.go('back')
      }
       cy.get(gobalLocaters.reportViews.gridreport).eq(0).click()
    cy.get(gobalLocaters.reportViews.multiviewbutton).click()
    cy.get(gobalLocaters.reportViews.multiviewenablebutton).click()
    cy.get(gobalLocaters.reportViews.splitview).should('exist')
    cy.get(gobalLocaters.reportViews.splitviewreports)
    .should('have.length', multiview)
      cy.get(gobalLocaters.reportViews.multiviewRefreshSchedule).click()
      cy.get(gobalLocaters.reportViews.multiviewFrequencyBox).click()
      cy.get(gobalLocaters.reportViews.multiviewSelectFrequency).eq(0).click()
      cy.get(gobalLocaters.reportViews.multiViewFrequencyButton).click()
      cy.wait(60000);
      cy.intercept("POST", "**/com.zenoptics.services/jaxrs/AddUsageData**").as("refreshApi");
      cy.wait("@refreshApi", { timeout: 10000 }).then(interception => {
        expect(interception.response.statusCode).to.equal(200);
      });
    cy.get(gobalLocaters.reportViews.toastNotification).should('have.text', ' Refresh initiated ')
      
  }


export function verifyMultiViewScheduleValidate(reportnumber, multiview) {
  cy.get(gobalLocaters.reportViews.gridView).click()
  cy.get(gobalLocaters.reportViews.gridContainer).should('exist')

  let firstReportNames = []; // Array to store report names

  // Capture report names for the first 5 elements
  for (let i = 0; i <=reportnumber ; i++) {
      cy.get(gobalLocaters.reportViews.gridreport).eq(i).click()
      cy.get('.mat-tooltip-trigger.report-title').invoke('text').then(reportName => {
          // Store the report name in the array
          firstReportNames.push(reportName);
          // Use the captured reportName as needed
          cy.log(`Report Name: ${reportName}`);
      });
      cy.go('back')
  }

  // Continue with your existing code...

  cy.get(gobalLocaters.reportViews.gridreport).eq(0).click()
  cy.get(gobalLocaters.reportViews.multiviewbutton).click()
  cy.get(gobalLocaters.reportViews.multiviewenablebutton).click()
  cy.get(gobalLocaters.reportViews.splitview).should('exist')
  cy.get(gobalLocaters.reportViews.splitviewreports)
      .should('have.length', multiview)
  cy.get(gobalLocaters.reportViews.multiviewRefreshSchedule).click()
  cy.get(gobalLocaters.reportViews.multiviewFrequencyBox).click()
  cy.get(gobalLocaters.reportViews.multiviewSelectFrequency).eq(0).click()
  cy.get(gobalLocaters.reportViews.multiViewFrequencyButton).click()
  cy.wait(60000);
  cy.intercept("POST", "**/com.zenoptics.services/jaxrs/AddUsageData**").as("refreshApi");
  cy.wait("@refreshApi", { timeout: 10000 }).then(interception => {
      expect(interception.response.statusCode).to.equal(200);
  });
  cy.get(gobalLocaters.reportViews.toastNotification).should('have.text', ' Refresh initiated ')

  // Compare the first report names with the report names after refresh
  cy.get('.mat-tooltip-trigger.report-title.ellipsis').each(($el, index) => {
      expect($el.text()).to.equal(firstReportNames[index]);

  });
}

//added for reggression
export function verify_SearchField_BIGlossary(report_name) {
  cy.get(gobalLocaters.gobalSearch.biglossary_search).eq(0).click({force:true}).type(report_name).should('be.visible')
  cy.get(gobalLocaters.gobalSearch.select_item).eq(0).click(0)
}

export function verify_reportsdropdown() {
  cy.get(gobalLocaters.gobalSearch.report_dropdown).click({force:true})
  cy.get(gobalLocaters.gobalSearch.click_reports).eq(1).click()
  cy.wait(3000)
  cy.get(gobalLocaters.gobalSearch.report_dropdown).click({force:true})
  cy.get(gobalLocaters.gobalSearch.click_reports).eq(0).click()


}


export function verifyFavIconInSearch(reports) {
  cy.wait(2000)
 searchAndSelectFirstReport(reports)
        report.toggleReportsFavoritesOn()
        gobalSearch(reports)
     //gobalSearchResult(reports)
cy.get(gobalLocaters.gobalSearch.searchCardInfoIcon).click()
cy.get(gobalLocaters.gobalSearch.searchCardThumbnailFooterFavIcon).should('exist')
       
        
}

export function verify_Thumbnailview() {
  cy.get(gobalLocaters.reportViews.viewSelection)
 cy.get(gobalLocaters.reportViews.thumbnailView).click()
 cy.get(gobalLocaters.reportViews.thumbnailContainer).should('exist')
}

export function verify_ListView() {
  cy.get(gobalLocaters.reportViews.listView).click()
 cy.get(gobalLocaters.reportViews.listContainer).should('exist')
}

export function verify_GridView() {
  cy.get(gobalLocaters.reportViews.gridView).click()
 cy.get(gobalLocaters.reportViews.gridContainer).should('exist')
}

export function verify_scrollbottom_fav() {
  dashboard.openMenu()
  dashboard.clickSideMenuLink(data.sidelinks[1].Reports[0])
  dashboard.clickSideMenuLink(data.sidelinks[1].Reports[2])
  cy.get(gobalLocaters.reportViews.fav_section).scrollTo('bottom');
  cy.wait(2000)
  cy.get(gobalLocaters.reportViews.thumbnail_card).eq(24).should('be.visible')
}

export function verify_alignment_cat() {
  const categorySelector = 'section[class="zo-grid-container ng-star-inserted"] mat-card[class="mat-card mat-focus-indicator zo-grid-card action-card _mat-animation-noopable ng-star-inserted"]';

    // Get the list of category elements
    cy.get(categorySelector).then(($categories) => {
      // Ensure there are at least three categories for comparison
      expect($categories.length).to.be.at.least(3);

      // Iterate through each category to verify horizontal alignment
      for (let i = 0; i < $categories.length - 1; i++) {
        const currentCategory = $categories.eq(i);
        const nextCategory = $categories.eq(i + 1);
    
        // Log the positions and widths for debugging
        cy.log(`Current Category ${i} position: ${currentCategory.offset().left}`);
        cy.log(`Current Category ${i} width: ${currentCategory.outerWidth()}`);
        cy.log(`Next Category ${i} position: ${nextCategory.offset().left}`);
    
        // Adjust the calculation based on your layout
        //const expectedAlignment = currentCategory.offsetParent().offset().left + currentCategory.offset().left + currentCategory.outerWidth();
        //const actualAlignment = nextCategory.offset().left;
    
        // Use assertions to check the horizontal alignment criteria, adjust as needed
        //expect(actualAlignment, `Category ${i} should be aligned with the next category`).to.equal(expectedAlignment);
      }
    });
  }

  export function verify_quicksearch() {
    cy.get(gobalLocaters.quick_search.allReports).click()
    cy.get(gobalLocaters.quick_search.search_msg).should('be.visible')
    cy.wait(5000)

    //Click Source
    cy.get(gobalLocaters.quick_search.source).click()
    cy.get(gobalLocaters.quick_search.allsource).should('have.text', ' All Sources ')
    cy.wait(5000)

    //Click categories
    //cy.get(gobalLocaters.quick_search.categories).click()
    //cy.get(gobalLocaters.quick_search.allcategories).should('have.text', 'All Categories ')


  }
