import { faker } from '@faker-js/faker';

export function randomData() {
  const email = faker.internet.email();
  const firstName = faker.person.firstName();
  const lastName = faker.person.lastName();
  const password = faker.internet.password({length: 10})
  const phoneNumber = faker.phone.number('1234######');
  const zipcode = faker.number.int({ min: 12345, max: 23456 });
  

  return { email, firstName, lastName, phoneNumber, password, zipcode,};
}