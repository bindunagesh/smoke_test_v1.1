import * as dashboardLocators from "../locators/dashboard_locators"
import data from "../fixtures/sanityData.json"
import * as logins from'../Pages/logins_PO'
import * as gobalLocaters from "../locators/gobal_locators"
import { subCategoriesPage } from "../locators/categories_locators";
import { logToGlobalFile } from '../utilities/global_log'


export function openMenu(){
  logToGlobalFile(" Wait: 5000 milliseconds")
cy.wait(2000)
logToGlobalFile("Open Menu: Menu opened successfully") 
cy.get(dashboardLocators.leftNavMenu.getMenu).click();
    }

export function getsideMenuLinksText(){
  logToGlobalFile("Getting the text of all side menu links")
    return cy.get(dashboardLocators.leftNavMenu.getAllLinksText);
  }

export function clickSideMenuLink(linkText){
  logToGlobalFile("Getting the text of all side menu links")
    return cy.get(dashboardLocators.leftNavMenu.getAllLinksText)
    .contains(linkText)
    .click()
    .should('be.visible')
    .then(() => {
      logToGlobalFile(` Clicked on side menu link: ${linkText}`)
  });       
} 
//added

export function navigateToReportSections(sections) {
  sections.forEach((reportIndex) => {
    
    logins.loginUser2(Cypress.env(),data.login.username,data.login.password);
    logToGlobalFile("Login application: Login Successful")

      openMenu()

      clickSideMenuLink(data.sidelinks[1].Reports[0]);
      logToGlobalFile(` Click side menu link: ${data.sidelinks[1].Reports[0]}`)

      clickSideMenuLink(data.sidelinks[1].Reports[reportIndex]);
      logToGlobalFile(` Click side menu link: ${data.sidelinks[1].Reports[reportIndex]}`)

      cy.wait(4000);
      logToGlobalFile("Wait: 4000 milliseconds")

      cy.get(dashboardLocators.reportheaders).should('be.visible')
      logToGlobalFile("Check visibility: Report headers are visible")

      cy.go('back');
      logToGlobalFile("Navigate: Go back to the dashboard")
      
      cy.get(dashboardLocators.dashboardheader).should('be.visible')
      logToGlobalFile("Check visibility: Dashboard header is visible");

      cy.reload();
      logToGlobalFile("Reload page: Page reloaded successfully")
      cy.task('taskLogToTerminalReport', `${formattedDate} Reload page: Page reloaded successfully`);
      
  });
}
//added
export function navigateToDashboardSections(sections) {
  sections.forEach((dasboardIndex) => {
    logins.loginUser2(Cypress.env(),data.login.username,data.login.password);
    logToGlobalFile("Login application: Login Successful")

      openMenu()


      clickSideMenuLink(data.sidelinks[dasboardIndex]);
      logToGlobalFile(` Click side menu link: ${data.sidelinks[dasboardIndex]}`)
      cy.wait(4000);
      logToGlobalFile("Wait: 4000 milliseconds")
      //cy.get(dashboardLocators.reportheaders).should('be.visible')
      cy.go('back');
      logToGlobalFile("Navigate: Go back to the dashboard");
      //cy.get(dashboardLocators.dashboardheader).should('be.visible')
      cy.reload();
      logToGlobalFile("Reload page: Page reloaded successfully")
      //cy.get(dashboardLocators.dashboardheader).should('be.visible')
  });
}
//added for regression
export function openBiGlossary() {
  openMenu()
  clickSideMenuLink(data.sidelinks[4])
  logToGlobalFile(`${formattedDate} Click side menu link: ${data.sidelinks[4]}`)
}


export function verifysideMenu(sidemenu){
    cy.wait(2000)
    logToGlobalFile("Wait: 2000 milliseconds")
    //cy.get(dashboardLocators.leftNavMenu.getAllLinksText)
      //.should("be.visible")
      //.and("have.length")
      //.each(($el, index) => {
        
        //expect($el).to.contain(sidemenu.sidelinks[index]);
        //const elementText = $el.text();
        //const expectedText = sidemenu.sidelinks[index];
        //expect(elementText).to.contain(expectedText);
        
      //});
    cy.fixture('sidemenu.json').as('sidemenu');
    cy.get('@sidemenu').then((sidemenu) => {
      // Access the 'sidelinks' array from the 'sidemenu' object
      const sidelinks = sidemenu.sidelinks;
    
      cy.get(dashboardLocators.leftNavMenu.getAllLinksText)
        .should("be.visible")
        .each(($el, index) => {
          expect($el).to.contain(sidelinks[index]);
          logToGlobalFile("Side menu links verified successfully")
        });
    });
      
}        

export function verifySideMenuLinks(navbarlinks) {
            cy.wrap(navbarlinks).each(({ linkText, url }) => {
              logToGlobalFile(` Clicking on navbar link: ${linkText}`)
                cy.get(dashboardLocators.leftNavMenu.getAllLinksText).contains(linkText).click();
                cy.wait(1500);
                logToGlobalFile(`Verifying URL includes: ${url}`)
        
                cy.url().should("include", url);
                // cy.go('back');
                cy.wait(1500);
            });
        }   

export function clickHomeIcon() {
  logToGlobalFile("Click on Home Icon")
  
            cy.get(dashboardLocators.homeIcon).click()           
        } 


export function verifyFavoriteInDashboard(reportname) {
            clickHomeIcon()
            cy.get(dashboardLocators.cardTitleText)
            .contains(dashboardLocators.favoriteText)
            .parentsUntil(dashboardLocators.cardTitleText)
            .parent(dashboardLocators.carouselElement)
            .find(dashboardLocators.favDisplay).eq(0)
            .should('have.text',reportname)
            logToGlobalFile(` Checked favorite display for report: ${reportname}`)
      
           }  


 export function clickFavoritesCarouselLink(){
              cy.get(dashboardLocators.cardTitleText)
              .contains(dashboardLocators.favoriteText).click()
        }   
        
 export function clickNotifications(){
  logToGlobalFile("Clicking on Notification")
  
            cy.get(dashboardLocators.notification).click()   
      }   

 export function clickNotificationPrivateComment(){
  logToGlobalFile("Clicking on Private Comments")
  
        cy.get(dashboardLocators.notificationPrivateComment).click()
        cy.get(dashboardLocators.clickPrivateComment).click()
        
  }   

  export function clickProfile(){
    logToGlobalFile("Clicking on Profile icon")

    cy.get(dashboardLocators.profile).click()
    
}   

export function dashboardSetting(){
  logToGlobalFile("Clicking on dashboard setting icon")
 
    cy.get(dashboardLocators.dashboardSetttingicon).click()
}

export function logout(){
  logToGlobalFile("Clicking on Logout")
  
    cy.get(dashboardLocators.logout).contains('Logout').click()
}
    

export function toggleDashboardStyle() {
    cy.get('mat-radio-button[value="classic"]').then(($checkbox)=>{
      if($checkbox.hasClass("mat-radio-checked")){
       cy.get('mat-radio-button[value="modern"]').click()
       logToGlobalFile("Switching from 'classic' to 'modern'")
       cy.wait(800)
       cy.get('.dashboard-setting__footer--save-btn').click()
       logToGlobalFile("Clicking on Save button")
     cy.get('#toast-container>div>div').should('exist')
     .and('have.text', ' User preferences saved successfully ')
     cy.get('.dashboard>.simple-page-scroller').should('exist')
     logToGlobalFile("Verifying the existence of the dashboard scroller")
     cy.screenshot('pass-test-34_1')
      }
     else{
       cy.get('mat-radio-button[value="classic"]').click()
       logToGlobalFile("Switching from 'modern' to 'classic'")
       cy.wait(800)
       cy.get('.dashboard-setting__footer--save-btn').click()
       logToGlobalFile("Clicking on Save button")
     cy.get('#toast-container>div>div').should('exist')
     .and('have.text', ' User preferences saved successfully ') 
     cy.get('.dashboard>.ng-star-inserted').should('exist')
     logToGlobalFile("Verifying the existence of the dashboard container")
     cy.screenshot('pass-test-34_2')
  
     }
     })
         
          
     clickNotifications  }


  export function toggleModernDashboard() {
    cy.get('mat-radio-button[value="classic"]').then(($checkbox)=>{
      if($checkbox.hasClass("mat-radio-checked")){
       cy.get('mat-radio-button[value="modern"]').click()
       cy.wait(800)
       cy.get('.dashboard-setting__footer--save-btn').click()
     cy.get('#toast-container>div>div').should('exist')
     .and('have.text', ' User preferences saved successfully ')
     cy.get('.dashboard>.simple-page-scroller').should('exist')
      }
    else{
        cy.get('.dashboard-setting__footer--cancel-btn').click()
    }
     })
         
          
  }

  export function verify_scolloption() {
    toggleDashboardStyle()
    openMenu()
    clickSideMenuLink(data.sidelinks[1].Reports[0]);
    clickSideMenuLink(data.sidelinks[1].Reports[2]);
    
    

  }

  export function pinUnpinWFFromDash(){
    cy.intercept('GET', '**/jaxrs/GetWorkflows**').as('getWorkflows');
    cy.get('.simple-page-scroller .icon-content-title').contains('Workflows').click();
    cy.wait('@getWorkflows').then((interception) => {
      const response = interception.response.body;
      const WorkflowName = response[0].AnalysisName;
    cy.get('#Home').click()
  ///pin to dash
  cy.get('.categorys-card').contains(WorkflowName)
  .closest('.categorys-card').children().eq(1).click()
  cy.get('.pin-workflow-dropdown>a').eq(1).click()
  cy.get('#toast-container>div>div').should('have.text', ` ${WorkflowName} is pinned to dashboard successfully `)
  cy.wait(1000)
  //unpinfrom dash
  cy.get('.card-title.ng-star-inserted').contains(WorkflowName)
  .closest('.card-title.ng-star-inserted').children().children().eq(3).invoke('show').click()
  cy.get('body app-root a:nth-child(2)').click()
  cy.get('#toast-container>div>div').should('have.text', ` ${WorkflowName} is unpinned from dashboard successfully `)
  
})
  }

  export function pinUnpinSubCatFromDash() {
    let subcategoryName
    cy.intercept('GET', '**/analytics-catalog/categories**').as('getCategories');
    cy.get('.simple-page-scroller .icon-content-title').contains('Categories').click();
    cy.wait('@getCategories').then((interception) => {
    const response = interception.response.body;
    const categoryName = response.categories[0].categoryName;
    cy.get('#Home').click()
    cy.get('.categorys-card').contains(categoryName).click()
    cy.get(".mat-tooltip-trigger.zo-grid-head-name").eq(0).invoke('text').then((text) => {
      subcategoryName = text;
      cy.get('#Home').click()
      cy.get('.categorys-card').contains(subcategoryName)
  .closest('.categorys-card').children().eq(1).invoke('show').click()
  cy.get('.pin-workflow-dropdown>a').click()
  cy.get('#toast-container>div>div').should('have.text', ` ${subcategoryName} is pinned to dashboard successfully `)
  cy.wait(2000)
  
  cy.get('.card-title.ng-star-inserted').contains(subcategoryName)
  .closest('.card-title.ng-star-inserted').children().eq(3).invoke('show').click()
  cy.get("a[class='hide-widget-content ng-star-inserted']").click()
  cy.get('#toast-container>div>div').should('have.text', ` ${subcategoryName} is unpinned from dashboard successfully `)

})
})
  }

export function unpinCatFromDash(){
  cy.intercept('GET', '**/analytics-catalog/categories**').as('getCategories');
  cy.get('.simple-page-scroller .icon-content-title').contains('Categories').click();
  cy.wait('@getCategories').then((interception) => {
  const response = interception.response.body;
  const categoryName = response.categories[0].categoryName;
  cy.get('#Home').click()
  
  cy.get('.card-title.ng-star-inserted').contains(categoryName)
  .closest('.card-title.ng-star-inserted').children().eq(3).invoke('show').click()
  cy.get("a[class='hide-widget-content ng-star-inserted']").click()
  cy.get('#toast-container>div>div').should('have.text', ` ${categoryName} is unpinned from dashboard successfully `)
   

})
}

  export function pinCatFromDash(){
    cy.intercept('GET', '**/analytics-catalog/categories**').as('getCategories');
  cy.get('.simple-page-scroller .icon-content-title').contains('Categories').click();
  cy.wait('@getCategories').then((interception) => {
  const response = interception.response.body;
  const categoryName = response.categories[0].categoryName;
  cy.get('#Home').click()
  
  cy.get('.categorys-card').contains(categoryName)
  .closest('.categorys-card').children().eq(1).click()
  cy.get('.pin-workflow-dropdown>a').click()
  cy.get('#toast-container>div>div').should('have.text', ` ${categoryName} is pinned to dashboard successfully `)
   

})
  }
  export function pinUnpinCatsFromWF() {
    cy.intercept('GET', '**/jaxrs/GetWorkflows**').as('getWorkflows');
    cy.get('.simple-page-scroller .icon-content-title').contains('Workflows').click();
    cy.wait('@getWorkflows').then((interception) => {
      const response = interception.response.body;
      const WorkflowName = response[0].AnalysisName;
      
  
    cy.get("div[class='pin-wf-container']").eq(0)
    //cy.get("span[class='pin-icon ng-star-inserted']").eq(0).click({force:true})
    .children().eq(1)
    .find('[data-mat-icon-name="ellipsis-icon"]').click({force:true})
    cy.get('.pin-workflow-link.ng-star-inserted').then(($pin)=>{
     const text = $pin.text()

     if (text === 'Unpin Workflow') {
      cy.get('.pin-workflow-link.ng-star-inserted').click()
      cy.get('#toast-container>div>div').should('have.text', ` ${WorkflowName} is unpinned from dashboard successfully `)
      cy.get('#Home').click()
      cy.get('.mat-tooltip-trigger.text-ellipsis').contains(WorkflowName).should('not.be.visible');
      cy.get('.simple-page-scroller .icon-content-title').contains('Workflows').click();
      cy.get("div[class='pin-wf-container']").eq(0)
    //cy.get("span[class='pin-icon ng-star-inserted']").eq(0).click({force:true})
    .children().eq(1)
    .find('[data-mat-icon-name="ellipsis-icon"]').click({force:true})
    cy.get('.pin-workflow-link.ng-star-inserted').click()
    cy.get('#toast-container>div>div').should('have.text', ` ${WorkflowName} is pinned to dashboard successfully `)
      cy.get('#Home').click()
      cy.get('.mat-tooltip-trigger.text-ellipsis').contains(WorkflowName).should('be.visible');
    }else{
      cy.get('.pin-workflow-dropdown>a').click()
      cy.get('#toast-container>div>div').should('have.text', ` ${WorkflowName} is pinned to dashboard successfully `)
      cy.get('#Home').click()
      cy.get('.mat-tooltip-trigger.text-ellipsis').contains(WorkflowName).should('be.visible')
      cy.get('.simple-page-scroller .icon-content-title').contains('Workflows').click();
      cy.get("div[class='pin-wf-container']").eq(0)
    //cy.get("span[class='pin-icon ng-star-inserted']").eq(0).click({force:true})
    .children().eq(1)
    .find('[data-mat-icon-name="ellipsis-icon"]').click({force:true})
    cy.get('.pin-workflow-link.ng-star-inserted').click()
    cy.get('#toast-container>div>div').should('have.text', ` ${WorkflowName} is unpinned from dashboard successfully `)
    cy.get('#Home').click()
    cy.get('.mat-tooltip-trigger.text-ellipsis').contains(WorkflowName)
    }
    
    
    
    })

  
  })
  }


  export function pinUnpinCatsFromCatPage() {
    cy.intercept('GET', '**/analytics-catalog/categories**').as('getCategories');
    cy.get('.simple-page-scroller .icon-content-title').contains('Categories').click();
    cy.wait('@getCategories').then((interception) => {
      const response = interception.response.body;
      const categoryName = response.categories[0].categoryName;
  
    cy.get('.zo-grid-details .zo-grid-head').eq(0)
    .children().eq(2)
    .find('[data-mat-icon-name="ellipsis-icon"]').click()
    cy.get('.pin-workflow-dropdown>a').then(($pin)=>{
     const text = $pin.text()

     if (text === 'Unpin Category') {
      cy.get('.pin-workflow-dropdown>a').click()
      cy.get('#toast-container>div>div').should('have.text', ` ${categoryName} is unpinned from dashboard successfully `)
      cy.get('#Home').click()
      cy.get('.simple-page-scroller .icon-content-title').contains(categoryName).should('not.exist');
      cy.get('.simple-page-scroller .icon-content-title').contains('Categories').click();
      cy.get('.zo-grid-details .zo-grid-head').eq(0)
    .children().eq(2)
    .find('[data-mat-icon-name="ellipsis-icon"]').click()
    cy.get('.pin-workflow-dropdown>a').click()
    cy.get('#toast-container>div>div').should('have.text', ` ${categoryName} is pinned to dashboard successfully `)
      cy.get('#Home').click()
      cy.get('.simple-page-scroller .icon-content-title').contains(categoryName).should('exist');
    }else{
      cy.get('.pin-workflow-dropdown>a').click()
      cy.get('#toast-container>div>div').should('have.text', ` ${categoryName} is pinned to dashboard successfully `)
      cy.get('#Home').click()
      cy.get('.simple-page-scroller .icon-content-title').contains(categoryName).should('exist');
      cy.get('.simple-page-scroller .icon-content-title').contains('Categories').click();
      cy.get('.zo-grid-details .zo-grid-head').eq(0)
    .children().eq(2)
    .find('[data-mat-icon-name="ellipsis-icon"]').click()
    cy.get('.pin-workflow-dropdown>a').click()
    cy.get('#toast-container>div>div').should('have.text', ` ${categoryName} is unpinned from dashboard successfully `)
    cy.get('#Home').click()
    cy.get('.simple-page-scroller .icon-content-title').contains(categoryName).should('not.exist');
    }
    
    
    
    })

  
  })
  }

   export function pinUnpinCatsFromSubCatPage() {
    let subcategoryName
    cy.intercept('GET', '**/analytics-catalog/categories**').as('getCategories');
    cy.get('.simple-page-scroller .icon-content-title').contains('Categories').click();
    cy.wait('@getCategories').then((interception) => {
    const response = interception.response.body;
    const categoryName = response.categories[0].categoryName;
    cy.get('#Home').click()
    cy.get('.categorys-card').contains(categoryName).click()
    cy.get(".mat-tooltip-trigger.zo-grid-head-name").eq(0).invoke('text').then((text) => {
      subcategoryName = text;
    
    cy.get('.zo-grid-details .zo-grid-head').eq(0)
    .children().eq(2)
    .find('[data-mat-icon-name="ellipsis-icon"]').click()
    cy.get('.pin-workflow-dropdown>a').then(($pin)=>{
     const text = $pin.text()

     if (text === 'Unpin Category') {
      cy.get('.pin-workflow-dropdown>a').click()
      cy.get('#toast-container>div>div').should('have.text', ` ${subcategoryName} is unpinned from dashboard successfully `)
      cy.get('#Home').click()
      cy.get('.simple-page-scroller .icon-content-title').contains(subcategoryName).should('not.exist');
      cy.get('.simple-page-scroller .icon-content-title').contains('Categories').click();
      cy.get(".mat-card").contains(categoryName).click()
      //cy.get(".mat-card").contains(subcategoryName)
      cy.get('.zo-grid-details .zo-grid-head').eq(0)
    .children().eq(2)
    .find('[data-mat-icon-name="ellipsis-icon"]').click()
    cy.get('.pin-workflow-dropdown>a').click()
    cy.get('#toast-container>div>div').should('have.text', ` ${subcategoryName} is pinned to dashboard successfully `)
      cy.get('#Home').click()
      cy.get('.simple-page-scroller .icon-content-title').contains(subcategoryName).should('exist');
    }else{
      cy.get('.pin-workflow-dropdown>a').click()
      cy.get('#toast-container>div>div').should('have.text', ` ${subcategoryName} is pinned to dashboard successfully `)
    cy.get('#Home').click()
      cy.get('.simple-page-scroller .icon-content-title').contains(subcategoryName).should('exist');
      cy.get('.simple-page-scroller .icon-content-title').contains('Categories').click();
      cy.get(".mat-card").contains(categoryName).click()
    cy.get('.zo-grid-details .zo-grid-head').eq(0)
    .children().eq(2)
  .find('[data-mat-icon-name="ellipsis-icon"]').click()
    cy.get('.pin-workflow-dropdown>a').click()
  cy.get('#toast-container>div>div').should('have.text', ` ${subcategoryName} is unpinned from dashboard successfully `)
    cy.get('#Home').click()
  cy.get('.simple-page-scroller .icon-content-title').contains(subcategoryName).should('not.exist');
  }
    
    
    
    })
  })

  
  })
  }


  export function verifyInMyReport(){
    
          
          // Report validation in grid View 
          //cy.get('/html/body/app-root/div[1]/app-side-nav/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/section[1]/app-list/div/div/div[1]/div[2]/div[3]/mat-icon[1]').click()
          cy.get(gobalLocaters.reportViews.viewSelection)
          cy.get(gobalLocaters.reportViews.gridView).click()
          cy.get(gobalLocaters.reportViews.gridContainer).should('exist')
          cy.get('.zo-report-grid-container').should('be.visible','2023 Info')
  
          //Report validation in List View
          cy.get(gobalLocaters.reportViews.viewSelection)
          cy.get(gobalLocaters.reportViews.listView).click()
          cy.get(gobalLocaters.reportViews.listContainer).should('exist')
          cy.get('.zo-report-list-container').should('be.visible','2023 Info')
  
          // Report validation in Thumbnail View
          cy.get(gobalLocaters.reportViews.viewSelection)
          cy.get(gobalLocaters.reportViews.thumbnailView).click()
          cy.get(gobalLocaters.reportViews.thumbnailContainer).should('exist')
          cy.get('.thumbnail-reports-container').should('be.visible','2023 Info')
  
          // for i button 
          //cy.get('/html/body/app-root/div[1]/app-side-nav/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/section[1]/app-list/div/div/div[2]/div/section/div/div[2]/app-thumbnail-card/div/div[2]/div[2]/mat-icon[2]').click({force:true})
          // certified icon
          //cy.get('/html/body/app-root/div[1]/app-side-nav/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/section[1]/app-list/div/div/div[2]/div/section/div/div[2]/app-thumbnail-card/div/div[1]/mat-icon[1]').should('be.visible')
  
  }
             
  export function verifyReportInFav(){
    // click on the menu
    cy.get("mat-icon[class='mat-icon notranslate menu-icon header-icons-for-common mat-icon-no-color ng-star-inserted']").click()
          cy.wait(1000)
          // click on the My Report 
          cy.get("mat-icon[data-mat-icon-name='My Reports']").eq(0).click()
          cy.wait(1000)
          //click Favorite
          cy.get("span[class='side-menu-icon-text sub-menu-text']").eq(1).click()
          cy.wait(3000)
          
          // Report validation in grid View 
          //cy.get('/html/body/app-root/div[1]/app-side-nav/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/section[1]/app-list/div/div/div[1]/div[2]/div[3]/mat-icon[1]').click()
          cy.get(gobalLocaters.reportViews.viewSelection)
          cy.get(gobalLocaters.reportViews.gridView).click()
          cy.get(gobalLocaters.reportViews.gridContainer).should('exist')
          cy.get('.zo-report-grid-container').should('be.visible','2023 Info')
  
          //Report validation in List View
          cy.get(gobalLocaters.reportViews.viewSelection)
          cy.get(gobalLocaters.reportViews.listView).click()
          cy.get(gobalLocaters.reportViews.listContainer).should('exist')
          cy.get('.zo-report-list-container').should('be.visible','2023 Info')
  
          // Report validation in Thumbnail View
          cy.get(gobalLocaters.reportViews.viewSelection)
          cy.get(gobalLocaters.reportViews.thumbnailView).click()
          cy.get(gobalLocaters.reportViews.thumbnailContainer).should('exist')
          cy.get('.thumbnail-reports-container').should('be.visible','2023 Info')
  
          // for i button 
  
          //cy.get('/html/body/app-root/div[1]/app-side-nav/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/section[1]/app-list/div/div/div[2]/div/section/div/div[2]/app-thumbnail-card/div/div[2]/div[2]/mat-icon[2]').click({force:true})
          // certified icon
          //cy.get('/html/body/app-root/div[1]/app-side-nav/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/section[1]/app-list/div/div/div[2]/div/section/div/div[2]/app-thumbnail-card/div/div[1]/mat-icon[1]').should('be.visible')
  
  }
  
  export function verifyReportInWhatsNew(){
          cy.get("mat-icon[class='mat-icon notranslate menu-icon header-icons-for-common mat-icon-no-color ng-star-inserted']").click()
          cy.wait(1000)
          cy.get("mat-icon[data-mat-icon-name='My Reports']").eq(0).click()
          cy.wait(1000)
          cy.get("span[class='side-menu-icon-text sub-menu-text']").eq(2).click()
          cy.wait(3000)
          //cy.xpath("/html/body/app-root/div[1]/app-side-nav/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/section[1]/app-list/div/div/div[2]/div/section/mat-card[1]").should('have.text', 'Sheet 1')
          //cy.xpath('/html/body/app-root/div[1]/app-side-nav/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/section[1]/app-list/div/div/div[1]/div[2]/div[3]/mat-icon[3]').click()
          
          // Report validation in grid View 
          //cy.get('/html/body/app-root/div[1]/app-side-nav/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/section[1]/app-list/div/div/div[1]/div[2]/div[3]/mat-icon[1]').click()
          cy.get(gobalLocaters.reportViews.viewSelection)
          cy.get(gobalLocaters.reportViews.gridView).click()
          cy.get(gobalLocaters.reportViews.gridContainer).should('exist')
          cy.get('.zo-report-grid-container').should('be.visible','2023 Info')
  
          //Report validation in List View
          cy.get(gobalLocaters.reportViews.viewSelection)
          cy.get(gobalLocaters.reportViews.listView).click()
          cy.get(gobalLocaters.reportViews.listContainer).should('exist')
          cy.get('.zo-report-list-container').should('be.visible','2023 Info')
  
          // Report validation in Thumbnail View
          cy.get(gobalLocaters.reportViews.viewSelection)
          cy.get(gobalLocaters.reportViews.thumbnailView).click()
          cy.get(gobalLocaters.reportViews.thumbnailContainer).should('exist')
          cy.get('.thumbnail-reports-container').should('be.visible','2023 Info')
  
          // for i button 
          //cy.get('/html/body/app-root/div[1]/app-side-nav/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/section[1]/app-list/div/div/div[2]/div/section/div/div[2]/app-thumbnail-card/div/div[2]/div[2]/mat-icon[2]').click({force:true})
          // certified icon
          //cy.get('/html/body/app-root/div[1]/app-side-nav/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/section[1]/app-list/div/div/div[2]/div/section/div/div[2]/app-thumbnail-card/div/div[1]/mat-icon[1]').should('be.visible')
  
  }
  
  export function verifyReportInRecentlyViewed(){
          cy.get("mat-icon[class='mat-icon notranslate menu-icon header-icons-for-common mat-icon-no-color ng-star-inserted']").click()
          cy.wait(1000)
          cy.get("mat-icon[data-mat-icon-name='My Reports']").eq(0).click()
          cy.wait(1000)
          cy.get("span[class='side-menu-icon-text sub-menu-text']").eq(3).click()
          cy.wait(3000)
          //cy.xpath("/html/body/app-root/div[1]/app-side-nav/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/section[1]/app-list/div/div/div[2]/div/section/mat-card[1]").should('have.text', 'Sheet 1')
          //cy.xpath('/html/body/app-root/div[1]/app-side-nav/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/section[1]/app-list/div/div/div[1]/div[2]/div[3]/mat-icon[3]').click()
  
          // Report validation in grid View 
          //cy.get('/html/body/app-root/div[1]/app-side-nav/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/section[1]/app-list/div/div/div[1]/div[2]/div[3]/mat-icon[1]').click()
          cy.get(gobalLocaters.reportViews.viewSelection)
          cy.get(gobalLocaters.reportViews.gridView).click()
          cy.get(gobalLocaters.reportViews.gridContainer).should('exist')
          cy.get('.zo-report-grid-container').should('be.visible','2023 Info')
  
          //Report validation in List View
          cy.get(gobalLocaters.reportViews.viewSelection)
          cy.get(gobalLocaters.reportViews.listView).click()
          cy.get(gobalLocaters.reportViews.listContainer).should('exist')
          cy.get('.zo-report-list-container').should('be.visible','2023 Info')
  
          // Report validation in Thumbnail View
          cy.get(gobalLocaters.reportViews.viewSelection)
          cy.get(gobalLocaters.reportViews.thumbnailView).click()
          cy.get(gobalLocaters.reportViews.thumbnailContainer).should('exist')
          cy.get('.thumbnail-reports-container').should('be.visible','2023 Info')
  
          // for i button 
          //cy.get('/html/body/app-root/div[1]/app-side-nav/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/section[1]/app-list/div/div/div[2]/div/section/div/div[2]/app-thumbnail-card/div/div[2]/div[2]/mat-icon[2]').click({force:true})
          // certified icon
          //cy.get('/html/body/app-root/div[1]/app-side-nav/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/section[1]/app-list/div/div/div[2]/div/section/div/div[2]/app-thumbnail-card/div/div[1]/mat-icon[1]').should('be.visible')
  
  
  }
  
  export function verifyInCategory(){
    openMenu()
    cy.get('.mat-list-item-content').eq(3).click()
    cy.get(':nth-child(14) > .zo-grid-details').click()
    cy.get(':nth-child(6) > .zo-grid-details').click()
  
    //cy.get('/html/body/app-root/div[1]/app-side-nav/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/section[1]/app-list/div/div/div[2]/section').should.contains('have.text','Test Site1').click()
    //cy.get('/html/body/app-root/div[1]/app-side-nav/div/mat-sidenav-container/mat-sidenav-content/mat-sidenav-container/mat-sidenav-content/div/section[1]/app-categories-list/div/div[2]/div/div/div[2]/section/mat-card[8]/div/div[1]/span[2]').should.contains('have.text','Dafault').click()
  
  
    }
  
  
  export function verifyFromRM(){
    openMenu()
    cy.get("mat-icon[data-mat-icon-name='My Reports']").eq(1).click()
    cy.get("span[class='side-menu-icon-text sub-menu-text']").eq(1).click()
    cy.get('#mat-input-1').type('2023 Info')
  
    if(cy.get('.table-container').contains('2023 Info')){
      cy.get('.cdk-column-businessOwner > .report-details-sub-type-value > span').should('have.value',' HRUSER USER ')
  
    } else{
      cy.screenshot()
    }
  
  }
  
  export function verifyFromRMForRG(){
    openMenu()
    cy.get("mat-icon[data-mat-icon-name='My Reports']").eq(1).click()
    cy.get("span[class='side-menu-icon-text sub-menu-text']").eq(1).click()
    cy.get('#mat-input-1').type('2023 Info')
    cy.get("span[class='report-details-sub-type-value zo-business-owner-name ng-star-inserted'] span").should('have.text', ' HRUSER USER ' )
    cy.get("span[class='zen-description-content'] span").should('have.text', 'Test')
  }