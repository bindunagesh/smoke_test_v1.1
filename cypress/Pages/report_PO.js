import * as reportLocators from "../locators/report_locators"
import * as gobal from "../Pages/gobal_PO"
import * as dashboard from "../Pages/dashboard_PO";
import data from "../fixtures/sanityData.json"
import * as setting from "../Pages/setting_PO"
import rgdata from "../fixtures/RG_Data.json"
import { loginUser2 } from "./logins_PO";
import * as logins from'../Pages/logins_PO'
import { logToGlobalFile } from '../utilities/global_log'

export function reportAbout() {
       logToGlobalFile('Click on Report About section');
        cy.get(reportLocators.report.getReportAbout).click()
}

export function reportRating(ratingReview) {
        cy.wait(1000)
        reportAbout()
        const arr = ratingReview.reportRating;
        for (const index in arr) {
                for (const [rating, comment] of Object.entries(arr[index])) {
                        reviewRating(rating, comment)
                        cy.wait(3000)
                }
        }
}

function reviewRating(rating, comment) {
        let rate = rating - 1
        cy.get(reportLocators.report.getRating).eq(rate).click()
        cy.get(reportLocators.report.getReportRatingComment).type(comment)
        cy.get(reportLocators.report.getReportSubmit).click()
        cy.get(reportLocators.report.reportToasterNotification).should('exist').and('have.text', ' Document rated successfully ')
        cy.get(reportLocators.report.reportRatingText).should('have.text', " " + rating + ".0 ")
        cy.get(reportLocators.report.reviewContent).should('have.text', ' ' + comment + ' ')
}

export function reportPublicComment(comment) {
        logToGlobalFile('Clicking on the report comment');
        cy.get(reportLocators.report.getReportComment).click()
        logToGlobalFile("Waiting: 3000 milliseconds");
        cy.wait(3000)
        logToGlobalFile("Getting the comment count before adding a new comment");
        commentCount().then(numBefore => {
                logToGlobalFile("Typing the comment in the comment input field");
                cy.get(reportLocators.report.getWriteComment).type(comment)
                logToGlobalFile("Clicking on the comment button");
                cy.get(reportLocators.report.getCommentButton).click()
               logToGlobalFile("Verifying the toaster notification for successful comment addition");
                cy.get(reportLocators.report.reportToasterNotification).should('exist').and('have.text', ' Comment Added Successfully ')
                logToGlobalFile("Wait:3000 milliseconds");
                cy.wait(3000)
                var numBefore = numBefore + 1
                logToGlobalFile("Checking if the comment count increased by 1");
                commentCount().then(numAfter => {
                        if (numBefore === numAfter) {
                                logToGlobalFile('comment count increased by 1')
                        }
                        else {
                                logToGlobalFile('no change in comment count')
                        }
                })
        })
}

function commentCount() {
        return cy.get(reportLocators.report.allComments)
                .then(($value) => {
                        const textValue = $value.text()
                        const str = textValue.replace((/^[0-9]+$/), '')
                        return Promise.resolve(Number(str.match(/\d+/)));
                })
}


export function reportPrivateComment(user, comment) {
        logToGlobalFile("Clicking on the report comment");
        cy.get(reportLocators.report.getReportComment).click()
        logToGlobalFile("Wait: 3000 milliseconds") 
        cy.wait(3000)
        logToGlobalFile("Getting the comment count before adding a new comment");
        commentCount().then(numBefore => {
                logToGlobalFile("Clicking on private") 
                cy.get(reportLocators.report.getPrivate).click()
                logToGlobalFile(`Clicking on search anf type ${user}`);
                cy.get(reportLocators.report.getsearchInput).type(user)
                cy.get(reportLocators.report.getsearchUserList).click()
                logToGlobalFile('Typing the comment in the comment input field');
                cy.get(reportLocators.report.getWriteComment).type(comment)
                logToGlobalFile("Clicking on the comment button")
                cy.get(reportLocators.report.getCommentButton).click()
                logToGlobalFile('Verifying the toaster notification for successful comment addition');
                cy.get(reportLocators.report.reportToasterNotification).should('exist').and('have.text', ' Comment Added Successfully ')
                logToGlobalFile('Wait: 6000 milliseconds');
                cy.wait(6000)
                var numBefore = numBefore + 1
                commentCount().then(numAfter => {
                        if (numBefore === numAfter) {
                                logToGlobalFile('comment count increased by 1')
                        }
                        else {
                                logToGlobalFile('no change in comment count')
                        }
                })
        })
}

export function verifyPrivateComment(user, comment){
        cy.get(reportLocators.report.clickcomment).click()
        cy.get(reportLocators.report.commentCreator).contains(user)
        cy.get(reportLocators.report.commentMessage).contains(comment)
}

export function numberOfWF() {
        cy.wait(6000)
        cy.visit(workflowUrl)
        return cy.get(reportLocators.report.numberOfWFTab)
                .then($value => {
                        const textValue = $value.text()
                        const numWF = textValue.replace(/[{()}]/g, '').slice(1, 3)
                        logToGlobalFile('Number of workflows ' + numWF)
                        return Promise.resolve(numWF)
                })
}


export function checkWorkflowPresent(workflowname) {
        cy.wait(6000)
        cy.visit(workflowUrl)
        //check the length of workflows   
        // numberOfWF()
        for (let i = 1; i < 5; i++) {
                const text = ':nth-child(' + [i] + ') > .flow-card > .flow-list-wrapper > .flow-card-header > .flow-card-header-name > .flow-name';
                const deleteWF = ':nth-child(' + [i] + ') > .flow-card > .flow-list-wrapper > .flow-footer > .flow-footer-right > [mattooltip="Delete Workflow"]';
                cy.get(text).then($value => {
                        const textValue = $value.text()
                        cy.wrap(textValue).as('wrapValue')
                        logToGlobalFile('value is : ' + textValue)
                        if (textValue === workflowname) {
                                logToGlobalFile('call delete WF')
                                cy.get(text).should('have.text', workflowname)
                                cy.get(deleteWF).click()
                                cy.get(reportLocators.report.deleteWorkflowButton).click()
                        } else {
                                logToGlobalFile('do not call delete WF')
                        }
                })
        }
}


export function createWorkflow(workflowname, user) {
        
        cy.get(reportLocators.report.getMyWorkspaceSaveButton).click()
        cy.get(reportLocators.report.getMyWorkspaceNextButton).click()
        cy.get(reportLocators.report.getWorkflowName).type(workflowname)
        cy.get(reportLocators.report.getWorkflowDescription).type(workflowname)
        cy.get(reportLocators.report.getWorkflowShareable).type(workflowname)
        cy.get(reportLocators.report.searchInput).type(user)
        cy.get(reportLocators.report.userList).eq(0).click()//added multiple true
        cy.get(reportLocators.report.saveWorkflowButton).click()
        cy.get(reportLocators.report.workflowToasterNotification).should('exist').and('have.text', ' Workflow created successfully ')
}
//added
export function getRandomWorkflow()
{
        dashboard.openMenu();
        dashboard.clickSideMenuLink(data.sidelinks[2])
}
//added
export function openRandomWorkflow_Reload(workflowname)
{
        dashboard.openMenu();
        dashboard.clickSideMenuLink(data.sidelinks[2])
        cy.get(reportLocators.report.workflowName).each(($workflowElement) => {
                cy.wrap($workflowElement).invoke('text').then((actualText) => {
                  logToGlobalFile('Actual Text:', actualText);
              
                  if (actualText === workflowname) {
                    // Assertion for a match
                    expect(actualText).to.equal(workflowname);
              
                    // Perform actions when a match is found
                    logToGlobalFile('Workflow name matched. Performing actions...');
                    cy.wrap($workflowElement).click({ force: true });
                  }
                });
              });
        cy.go('back')
        cy.reload()

}

export function openRandomWorkflow_(workflowname)
{
        dashboard.openMenu();
        dashboard.clickSideMenuLink(data.sidelinks[2])
        cy.get(reportLocators.report.workflowName).each(($workflowElement) => {
                cy.wrap($workflowElement).invoke('text').then((actualText) => {
                  logToGlobalFile('Actual Text:', actualText);
              
                  if (actualText === workflowname) {
                    // Assertion for a match
                    expect(actualText).to.equal(workflowname);
              
                    // Perform actions when a match is found
                    logToGlobalFile('Workflow name matched. Performing actions...');
                    cy.wrap($workflowElement).click({ force: true });
                  }
                });
              });

}



export function add_report_WF(workflowname, workflow1,workflow2) {
        let initialReportText;
    
        dashboard.openMenu();
        

        dashboard.clickSideMenuLink(data.sidelinks[2]);
        logToGlobalFile(`Clicked on side menu link: ${data.sidelinks[2]}`) 

    
        cy.get(reportLocators.report.workflowName).each(($workflowElement) => {
            cy.wrap($workflowElement).invoke('text').then((actualText) => {
                logToGlobalFile('Actual Text:', actualText);
    
                if (actualText === workflowname) {
                    expect(actualText).to.equal(workflowname);
    
                    logToGlobalFile('Workflow name matched. Performing actions...');
    
                    const $matCard = $workflowElement.parents('.mat-card');
    
                    if ($matCard.find("span[class='report']").length > 0) {
                        initialReportText = $matCard.find("span[class='report']").text().split('reports')[0].trim();
                        logToGlobalFile('Initial ReportCount:', initialReportText);
    
                        // Perform operation
                        performWorkflowActions_addReport($workflowElement, workflow1, workflow2);
    
                        // Check if the updated value is incremented by 2
                        checkUpdatedReportCount_addReport(workflowname, initialReportText);
                    } else {
                        logToGlobalFile('No span[class=\'report\'] found within .mat-card.');
                    }
                }
            });
        });
    }

export function remove_report_WF(workflowname) {
        let initialReportText;
    
        dashboard.openMenu();
        dashboard.clickSideMenuLink(data.sidelinks[2]);
    
        cy.get(reportLocators.report.workflowName).each(($workflowElement) => {
            cy.wrap($workflowElement).invoke('text').then((actualText) => {
                logToGlobalFile('Actual Text:', actualText);
    
                if (actualText === workflowname) {
                    expect(actualText).to.equal(workflowname);
    
                    logToGlobalFile('Workflow name matched. Performing actions...');
    
                    const $matCard = $workflowElement.parents('.mat-card');
    
                    if ($matCard.find("span[class='report']").length > 0) {
                        initialReportText = $matCard.find("span[class='report']").text().split('reports')[0].trim();
                        logToGlobalFile('Initial ReportCount:', initialReportText);
    
                        // Perform operation
                        performWorkflowActions_removeReport($workflowElement);

                        // Check if the updated value is incremented by 2
                        checkUpdatedReportCount_removeReport(workflowname, initialReportText);
                    } else {
                        logToGlobalFile('No span[class=\'report\'] found within .mat-card.');
                    }
                }
            });
        });
    }

export function share_report_WF_user(workflowname,user) {
        let initialShareCount;
    
        dashboard.openMenu();
        dashboard.clickSideMenuLink(data.sidelinks[2]);
    
        cy.get(reportLocators.report.workflowName).each(($workflowElement) => {
            cy.wrap($workflowElement).invoke('text').then((actualText) => {
                logToGlobalFile('Actual Text:', actualText);
    
                if (actualText === workflowname) {
                    expect(actualText).to.equal(workflowname);
    
                    logToGlobalFile('Workflow name matched. Performing actions...');
                    const $matCard = $workflowElement.parents('.mat-card');
                
                    if ($matCard.find("span[class='report']").length > 0) {
                        initialShareCount = $matCard.find("span[class='share-count']").text().split('Shares')[0].trim();
                        logToGlobalFile('Initial ShareCountCount:', initialShareCount);
    
                        // Perform operation
                        performWorkflowActions_shareReport_user($workflowElement, user);
                        checkUpdatedShareCount_shareReport_user(workflowname, initialShareCount);
                        
                    } else {
                        logToGlobalFile('No span[class=\'report\'] found within .mat-card.');
                    }
    
                    
    
                    
                }
            });
        });
    }
    
export function share_report_WF_group(workflowname,usergroup) {
        let initialShareCount;
    
        dashboard.openMenu();
        dashboard.clickSideMenuLink(data.sidelinks[2]);
    
        cy.get(reportLocators.report.workflowName).each(($workflowElement) => {
            cy.wrap($workflowElement).invoke('text').then((actualText) => {
                cy.log('Actual Text:', actualText);
    
                if (actualText === workflowname) {
                    expect(actualText).to.equal(workflowname);
    
                    cy.log('Workflow name matched. Performing actions...');
                    const $matCard = $workflowElement.parents('.mat-card');
                
                    if ($matCard.find("span[class='report']").length > 0) {
                        initialShareCount = $matCard.find("span[class='share-count']").text().split('Shares')[0].trim();
                        cy.log('Initial ShareCountCount:', initialShareCount);
    
                        // Perform operation
                        performWorkflowActions_shareReport_group($workflowElement, usergroup);
                        checkUpdatedShareCount_shareReport_group(workflowname, initialShareCount);
                        
                    } else {
                        cy.log('No span[class=\'report\'] found within .mat-card.');
                    }
    
                    
    
                    
                }
            });
        });
    }
    
    function performWorkflowActions_addReport($workflowElement, workflow1, workflow2) {
        const $matCard = $workflowElement.parents('.mat-card');
        
        cy.wrap($workflowElement).click({ force: true });
    
        cy.get(reportLocators.report.getMyWorkspaceSaveButton).click();
        cy.get(reportLocators.report.getMyWorkspaceNextButton).click();
        cy.get(reportLocators.report.getMyWorkspaceNextButton2).click();
        cy.get(reportLocators.report.getMyWorkspaceEditButton).click();
        cy.get(reportLocators.report.getMyWorkspaceAddMoreButton).click();
        cy.get(reportLocators.report.getMyWorkspaceSearch).clear().type(workflow1);
        cy.wait(2000);
        cy.get(reportLocators.report.getMyWorkspacecheckbox).eq(2).click({ force: true });
        cy.get(reportLocators.report.getMyWorkspaceAdd).click();
        cy.get(reportLocators.report.getMyWorkspaceAddMoreButton).click();
        cy.get(reportLocators.report.getMyWorkspaceSearch).clear().type(workflow2);
        cy.wait(2000);
        cy.get(reportLocators.report.getMyWorkspacecheckbox).eq(2).click({ force: true });
        cy.get(reportLocators.report.getMyWorkspaceAdd).click();
        cy.get(reportLocators.report.getMyWorkspaceUpdateBtn).click();
    
        cy.wait(5000);
    }
    
    function checkUpdatedReportCount_addReport(workflowname, initialReportText) {
        dashboard.clickHomeIcon();
        dashboard.openMenu();
        //dashboard.openMenu();
        dashboard.clickSideMenuLink(data.sidelinks[2]);
    
        cy.get(reportLocators.report.workflowName).each(($workflowElement) => {
            cy.wrap($workflowElement).invoke('text').then((actualText) => {
                cy.log('Actual Text:', actualText);
    
                if (actualText === workflowname) {
                    expect(actualText).to.equal(workflowname);
    
                    const $matCard = $workflowElement.parents('.mat-card');
    
                    if ($matCard.find("span[class='report']").length > 0) {
                        const updatedReportText = $matCard.find("div[class='flow-card-header-reports']").text().split('reports')[0].trim();
                        cy.log('Updated Report Count', updatedReportText);
    
                        const initialReportCount = parseInt(initialReportText);
                        const updatedReportCount = parseInt(updatedReportText);
    
                        expect(updatedReportCount).to.equal(initialReportCount + 2);
                    }
                }
            });
        });
    }

    function performWorkflowActions_removeReport($workflowElement) {
        const $matCard = $workflowElement.parents('.mat-card');
        
        cy.wrap($workflowElement).click({ force: true });
    
        cy.get(reportLocators.report.getMyWorkspaceSaveButton).click();
        cy.get(reportLocators.report.getMyWorkspaceNextButton).click();
        cy.get(reportLocators.report.getMyWorkspaceNextButton2).click();
        cy.get(reportLocators.report.getMyWorkspaceEditButton).click();
        cy.get(reportLocators.report.getMyWorkspaceRemove).eq(0).click()
        cy.get(reportLocators.report.getMyWorkspaceRemove).eq(1).click()
        cy.get(reportLocators.report.getMyWorkspaceUpdateBtn).click();
    
        cy.wait(5000);
    }
    
    function checkUpdatedReportCount_removeReport(workflowname, initialReportText) {
        dashboard.clickHomeIcon();
        dashboard.openMenu();
        //dashboard.openMenu();
        dashboard.clickSideMenuLink(data.sidelinks[2]);
    
        cy.get(reportLocators.report.workflowName).each(($workflowElement) => {
            cy.wrap($workflowElement).invoke('text').then((actualText) => {
                cy.log('Actual Text:', actualText);
    
                if (actualText === workflowname) {
                    expect(actualText).to.equal(workflowname);
    
                    const $matCard = $workflowElement.parents('.mat-card');
    
                    if ($matCard.find("span[class='report']").length > 0) {
                        const updatedReportText = $matCard.find("div[class='flow-card-header-reports']").text().split('reports')[0].trim();
                        cy.log('Updated Report Count', updatedReportText);
    
                        const initialReportCount = parseInt(initialReportText);
                        const updatedReportCount = parseInt(updatedReportText);
    
                        expect(updatedReportCount).to.equal(initialReportCount - 2);
                    }
                }
            });
        });
    }

    function performWorkflowActions_shareReport_user($workflowElement,user) {
        const $matCard = $workflowElement.parents('.mat-card');
        
        cy.wrap($workflowElement).click({ force: true });
    
        cy.get(reportLocators.report.getMyWorkspaceSaveButton).click();
        cy.get(reportLocators.report.getMyWorkspaceNextButton).click();
        cy.get(reportLocators.report.getMyWorkspaceNextButton2).click();
        cy.get(reportLocators.report.getMyWorkspaceEditButton).click();
        //cy.get(reportLocators.report.getMyWorkspaceShareBtn).click()
        cy.get(reportLocators.report.input_user).type(user)
        cy.get(reportLocators.report.input_userlist).eq(0).click()
        cy.get(reportLocators.report.getMyWorkspaceUpdateBtn).click();
    
        cy.wait(5000);
    }
    function checkUpdatedShareCount_shareReport_user(workflowname, initialShareCount) {
        dashboard.clickHomeIcon();
        dashboard.openMenu();
        //dashboard.openMenu();
        dashboard.clickSideMenuLink(data.sidelinks[2]);
    
        cy.get(reportLocators.report.workflowName).each(($workflowElement) => {
            cy.wrap($workflowElement).invoke('text').then((actualText) => {
                cy.log('Actual Text:', actualText);
    
                if (actualText === workflowname) {
                    expect(actualText).to.equal(workflowname);
    
                    const $matCard = $workflowElement.parents('.mat-card');
    
                    if ($matCard.find("span[class='report']").length > 0) {
                        const updatedShareCount = $matCard.find("span[class='share-count']").text().split('reports')[0].trim();
                        cy.log('Updated Report Count', updatedShareCount);
    
                        const initialReportCount = parseInt(initialShareCount);
                        const updatedReportCount = parseInt(updatedShareCount);
    
                        expect(updatedReportCount).to.equal(initialReportCount + 1);
                    }
                }
            });
        });
    }

    function performWorkflowActions_shareReport_group($workflowElement, usergroup) {

        const $matCard = $workflowElement.parents('.mat-card');
        
        cy.wrap($workflowElement).click({ force: true });
    
        cy.get(reportLocators.report.getMyWorkspaceSaveButton).click();
        cy.get(reportLocators.report.getMyWorkspaceNextButton).click();
        cy.get(reportLocators.report.getMyWorkspaceNextButton2).click();
        cy.get(reportLocators.report.getMyWorkspaceEditButton).click();
        //cy.get(reportLocators.report.getMyWorkspaceShareBtn).click()
        cy.get(reportLocators.report.input_user).type(usergroup)
        cy.get(reportLocators.report.input_userlist).eq(0).click()
        cy.get(reportLocators.report.getMyWorkspaceUpdateBtn).click();
        cy.wait(2000);
        cy.get('#toast-container>div>div').should('exist')
     //.and('have.text', ' Workflow shared Successfully ') 
     cy.screenshot('screenshot-014_1')
    }
    function checkUpdatedShareCount_shareReport_group(workflowname, initialShareCount) {
        dashboard.clickHomeIcon();
        dashboard.openMenu();
        //dashboard.openMenu();
        dashboard.clickSideMenuLink(data.sidelinks[2]);
        dashboard.openMenu();
    
        cy.get(reportLocators.report.workflowName).each(($workflowElement) => {
            cy.wrap($workflowElement).invoke('text').then((actualText) => {
                cy.log('Actual Text:', actualText);
    
                if (actualText === workflowname) {
                    expect(actualText).to.equal(workflowname);
    
                    const $matCard = $workflowElement.parents('.mat-card');
    
                    if ($matCard.find("span[class='report']").length > 0) {
                        const updatedShareCount = $matCard.find("span[class='share-count']").text().split('reports')[0].trim();
                        cy.log('Updated Report Count', updatedShareCount);
    
                        const initialReportCount = parseInt(initialShareCount);
                        const updatedReportCount = parseInt(updatedShareCount);
    
                        expect(updatedReportCount).to.equal(initialReportCount + data.workflow.usergroup_len);
                    }
                }
            });
        });
 }


    
    
    

export function deleteWorkflow_1(workflowname) {
      
        cy.get(reportLocators.report.workflowName).contains(workflowname).should('exist');
        cy.get(reportLocators.report.deleteWorkflow).click()
        cy.get(reportLocators.report.deleteWorkflowButton).click()
        cy.get(reportLocators.report.workflowToasterNotification).should('exist').and('have.text', ' ' + workflowname + ' workflow Deleted successfully ')
        
}

//added(modified function)
export function deleteWorkflow(workflowname) {
        cy.get(reportLocators.report.workflowName).each(($workflowElement) => {
          const workflow_name = $workflowElement.text();
      
          if (workflow_name == workflowname) {
            $workflowElement.click();
            //cy.get(reportLocators.report.deleteWorkflow).click();
            //cy.get(reportLocators.report.deleteWorkflowButton).click();
            //cy.get(reportLocators.report.workflowToasterNotification).should('have.text', ' ' + workflowname + ' workflow Deleted successfully ');
            
            
            return false;
          }
        });
}
export function thumbnailUserGenerate(username, password) {
        cy.wait(2000)
        cy.get(reportLocators.report.getGenerate).should('have.text','Generate').click()
        cy.get(reportLocators.report.getNext).should('have.text','Next').click()
       // cy.wait(1000)
        cy.get(reportLocators.report.getUserName).type(username)
        cy.wait(1000)
        cy.get(reportLocators.report.getPassword).type(password)
        cy.get(reportLocators.report.getGenerateOnCredentials).click()
        cy.get(reportLocators.report.reportToasterNotification).should('exist').and('have.text', ' Live Thumbnail generation initiated ')
        cy.get(reportLocators.report.getGenerate).should('have.text','Generate').click()
        cy.get(reportLocators.report.thumbnailLog).click()
        cy.get(reportLocators.report.thumbnailStatus).should('have.text', ' waiting ')
}
export function sendReportInEmail(reportname) {
        logToGlobalFile("Clicking on send Email icon") 
        cy.get(reportLocators.report.sendEmailIcon).click()
        logToGlobalFile("Type Email ID to send") 
        cy.get(reportLocators.report.writeEmailRecipient).type('amir@zenoptics.com')
        logToGlobalFile("Type email message 'please find below your report");
        cy.get(reportLocators.report.writeEmailMessage).type('please find below your report')
        logToGlobalFile("Click on send email button");
        cy.get(reportLocators.report.sendEmailButton).click()
        
  }

export function toggleReportSubscribeOn() {
        logToGlobalFile("Mat-tooltip attribute is 'Unsubscribe'. Calling unSubscribeReport.")
        cy.get(reportLocators.report.reportHeader)
          .find(reportLocators.report.reportHeaderBottomActionIcon)
          .children("mat-icon")
          .eq(2)
          .then(($icon) => {
            if ($icon.attr("mattooltip") === "Unsubscribe") {
                logToGlobalFile('Mat-tooltip attribute is "Unsubscribe". Calling unSubscribeReport.') 
              unSubscribeReport();
            }
          });
          logToGlobalFile("Calling subscribeReport.") 
        subscribeReport();
      }


export function unSubscribeReport(){
        cy.get(reportLocators.report.reportHeader)
        .find(reportLocators.report.reportHeaderBottomActionIcon)
        .children('mat-icon').eq(2)
        .click()
        cy.get(reportLocators.report.confirmationForYes)
        .should('have.text','Yes').click()
        cy.get(reportLocators.report.reportToastnotfication)
        .should('exist')
        .and('have.text',' Unsubscribed successfully ')
        cy.get(reportLocators.report.reportHeader)
        .find(reportLocators.report.reportHeaderBottomActionIcon)
        .children('mat-icon').eq(2).should('have.attr','mattooltip', 'Subscribe')
      
  }


  export function subscribeReport(){
        cy.get(reportLocators.report.reportHeader)
        .find(reportLocators.report.reportHeaderBottomActionIcon)
        .children('mat-icon').eq(2)
        .click()
        cy.get(reportLocators.report.confirmationForYes)
        .contains('Yes')
        cy.get(reportLocators.report.confirmationForYes).should('have.text','Yes').click()
        cy.get(reportLocators.report.reportToastnotfication).should('exist').and('have.text',' Subscribed successfully ')
        cy.get(reportLocators.report.reportHeader)
        .find(reportLocators.report.reportHeaderBottomActionIcon)
        .children('mat-icon').eq(2).should('have.attr','mattooltip', 'Unsubscribe')
      
  }


  export function favoriteReport(num){
        cy.get(reportLocators.report.reportHeader)
        .find(reportLocators.report.reportHeaderBottomActionIcon).eq(num)
        .children('mat-icon').eq(0)
        .click()
        cy.get(reportLocators.report.confirmationDailogForFav)
        .contains('Add')
        cy.get(reportLocators.report.confirmationForYes)
        .should('have.text','Yes, Proceed').click()
        cy.get(reportLocators.report.reportToastnotfication)
        .should('exist')
        .and('have.text',' Successfully added to favorites ')
        cy.get(reportLocators.report.reportHeader)
        .find(reportLocators.report.reportHeaderBottomActionIcon).eq(num)
        .children('mat-icon').eq(0).should('have.attr','mattooltip', 'Unfavorite')
      
  }

  export function unFavoriteReport(num){
        cy.get(reportLocators.report.reportHeader)
        .find(reportLocators.report.reportHeaderBottomActionIcon).eq(num)
        .children('mat-icon').eq(0)
        .click()
        cy.get(reportLocators.report.confirmationDailogForFav)
        .contains('Delete')
        cy.get(reportLocators.report.confirmationForYes)
        .should('have.text','Yes, Proceed').click()
        cy.get(reportLocators.report.reportToastnotfication)
        .should('exist')
        .and('have.text',' Successfully removed from favorites ')
        cy.get(reportLocators.report.reportHeader)
        .find(reportLocators.report.reportHeaderBottomActionIcon).eq(num)
        .children('mat-icon').eq(0).should('have.attr','mattooltip', 'Favorite')
      
  }


  export function toggleReportFavoritesOn(num) {
        logToGlobalFile(`Toggle report favorites on: ${num}`) 
    cy.get(reportLocators.report.reportHeader)
      .find(reportLocators.report.reportHeaderBottomActionIcon).eq(num)
      .children("mat-icon")
      .eq(0)
      .then(($icon) => {
        if ($icon.attr("mattooltip") === "Unfavorite") {
        logToGlobalFile(`Unfavoriting report: ${num}`);
        unFavoriteReport(num);
        }
      });
      logToGlobalFile(`Favoriting report: ${num}`);
      favoriteReport(num);
  }

////NEW//
export function favoriteReports (){
        cy.get(reportLocators.report.reportHeader)
        .find(reportLocators.report.reportHeaderBottomActionIcon)
        .children('mat-icon').eq(0)
        .click()
        cy.get(reportLocators.report.confirmationDailogForFav)
        .contains('Add')
        cy.get(reportLocators.report.confirmationForYes)
        .should('have.text','Yes, Proceed').click()
        cy.get(reportLocators.report.reportToastnotfication)
        .should('exist')
        .and('have.text',' Successfully added to favorites ')
        cy.get(reportLocators.report.reportHeader)
        .find(reportLocators.report.reportHeaderBottomActionIcon)
        .children('mat-icon').eq(0).should('have.attr','mattooltip', 'Unfavorite')
      
  }

  export function unFavoriteReports (){
        cy.get(reportLocators.report.reportHeader)
        .find(reportLocators.report.reportHeaderBottomActionIcon)
        .children('mat-icon').eq(0)
        .click()
        cy.get(reportLocators.report.confirmationDailogForFav)
        .contains('Delete')
        cy.get(reportLocators.report.confirmationForYes)
        .should('have.text','Yes, Proceed').click()
        cy.get(reportLocators.report.reportToastnotfication)
        .should('exist')
        .and('have.text',' Successfully removed from favorites ')
        cy.get(reportLocators.report.reportHeader)
        .find(reportLocators.report.reportHeaderBottomActionIcon)
        .children('mat-icon').eq(0).should('have.attr','mattooltip', 'Favorite')
      
  }


  export function toggleReportsFavoritesOn() {
        logToGlobalFile("Finding report header and navigating to the first child mat-icon");
    cy.get(reportLocators.report.reportHeader)
      .find(reportLocators.report.reportHeaderBottomActionIcon)
      .children("mat-icon")
      .eq(0)
      .then(($icon) => {
        logToGlobalFile("Checking if the mat-tooltip attribute is Unfavorite") 
        if ($icon.attr("mattooltip") === "Unfavorite") {
        logToGlobalFile("Clicking on Unfavourite report ");      
          unFavoriteReports();
        }
      });
      logToGlobalFile("Clicking on favourite report"); 
    favoriteReports();
  }
/////////////

  export function toggleReportsSubscribeOn() {
        cy.get(reportLocators.report.reportHeader)
          .find(reportLocators.report.reportHeaderBottomActionIcon)
          .children("mat-icon")
          .eq(2)
          .then(($icon) => {
            if ($icon.attr("mattooltip") === "Unsubscribe") {
              unSubscribeReport();
            }
          });
        subscribeReport();
      }

export function getRandomReport(){
        
        dashboard.openMenu();
        //dashboard.clickSideMenuLink(data.SIDELINKS_REPORTS.Reports)
        dashboard.clickSideMenuLink(data.sidelinks[1].Reports[0])//added code to avoid exception
        //cy.wait(50000) //added for srv15, its takimg too long to open myreports page
        cy.intercept('GET', '**/jaxrs/GetAllReports**').as('getAllReports')
        cy.get('.mat-drawer [class="mat-list-item-content"]').contains('My Reports').click()
        cy.wait('@getAllReports').then((interception) => {
          const response = interception.response.body;
          const reportName = response.reports[0].DISPLAY_VALUE;
          gobal.searchAndSelectFirstReport(reportName) 

        })
}
export function createWorkflow_multipleuser(workflowname, ...users) {
        let userCount = 0;
        logToGlobalFile("Clicking on save Button")
        cy.get(reportLocators.report.getMyWorkspaceSaveButton).click()
        logToGlobalFile("Clicking on next Button");
        cy.get(reportLocators.report.getMyWorkspaceNextButton).click()
        logToGlobalFile(`Type ${workflowname} to save workflow`);
        cy.get(reportLocators.report.getWorkflowName).type(workflowname)
        logToGlobalFile(`Type ${workflowname}`);
        cy.get(reportLocators.report.getWorkflowDescription).type(workflowname)
        logToGlobalFile(`Type ${workflowname}`);
        cy.get(reportLocators.report.getWorkflowShareable).type(workflowname)
        // Iterate over users
        

    for (const user of users) {
        logToGlobalFile(`Clicked on the report search field ${user}`);
        cy.get(reportLocators.report.search).click({force:true}).type(user);
        logToGlobalFile('Clicked on the first user in the user list');
        cy.get(reportLocators.report.userList).eq(0).click(); // Assuming the first user in the list
        logToGlobalFile(` Type ${workflowname}`)
        userCount++
        cy.log(userCount)
    }
    
        logToGlobalFile("Click save button") 
        cy.get(reportLocators.report.saveWorkflowButton).click()
        logToGlobalFile('Verified that the workflow toaster notification exists and has the expected text');
        
        cy.get(reportLocators.report.workflowToasterNotification).should('exist').and('have.text', ' Workflow created successfully ')
        
        return userCount;
}

export function share_wf(workflowname) {
        let foundMatch = false;
        dashboard.openMenu();
        dashboard.clickSideMenuLink(data.sidelinks[2])
        

cy.get(reportLocators.report.workflownames).each(($workflowElement) => {
  cy.wrap($workflowElement).invoke('text').then((actualText) => {
    cy.log('Actual Text:', actualText);

    if (actualText === workflowname) {
      foundMatch = true;
      logToGlobalFile(` Workflow name matched: ${workflowname}`)
            // Assertion for a match
      expect(actualText).to.equal(workflowname);
    }
  }).then(() => {
    // Check if a match has been found
    if (foundMatch) {
        //add code
    } else {
      // Continue to the next iteration
      logToGlobalFile("No match found. Continuing to the next workflowname.")
      cy.log('No match found. Continuing to the next workflowname.');
    }
  });
});

        cy.get(reportLocators.report.sharewf)
  .each(($shareWfButton, index, $shareWfButtons) => {
    // Find the corresponding .mat-card for each sharewf button
    const $matCard = $shareWfButtons.eq(index).parents('.mat-card');

    // Check if the .mat-card contains the workflow name
    if ($matCard.text().includes(workflowname)) {
        logToGlobalFile("Clicking the share button")
        
      // Click the sharewf button
      cy.wrap($shareWfButton).click();
      cy.get(reportLocators.report.email_sub).should('be.visible').type('Test Automation')
      cy.get(reportLocators.report.shr_btn).should('be.visible').click()
      //cy.get(reportLocators.report.workflowToasterNotification).should('have.text', 'Workflow Shared Successfully')
      logToGlobalFile("Shared workflow successfully")
}
  });


}


export function verify_sharecount(workflowname, userCount) {
        let foundMatch = false;
        dashboard.openMenu();
        dashboard.clickSideMenuLink(data.sidelinks[2])
        

cy.get(reportLocators.report.workflownames).each(($workflowElement) => {
  cy.wrap($workflowElement).invoke('text').then((actualText) => {
    cy.log('Actual Text:', actualText);

    if (actualText === workflowname) {
      foundMatch = true;
      // Assertion for a match
      expect(actualText).to.equal(workflowname);
    }
  }).then(() => {
    // Check if a match has been found
    if (foundMatch) {
        //add code
    } else {
      // Continue to the next iteration
      cy.task('taskLogToTerminalReport',`${formattedDate} No match found. Continuing to the next workflowname.`)
      
    }
  });
});

cy.wait(5000)

        cy.get(reportLocators.report.workflow_info)
  .each(($shareWfInfoButton, index, $shareWfInfoButtons) => {
    // Find the corresponding .mat-card for each sharewf button
    const $matCard = $shareWfInfoButtons.eq(index).parents('.mat-card');

    // Check if the .mat-card contains the workflow name
    if ($matCard.text().includes(workflowname)) {
      // Click the sharewf button
      cy.wrap($shareWfInfoButton).click();
      cy.get(reportLocators.report.workflow_infoames).its('length').then((numberOfValues) => {
        logToGlobalFile(`Number of values: ${numberOfValues}`)
        logToGlobalFile(`UserCount:'${userCount}`)
        cy.log(`Number of values: ${numberOfValues}`);
        cy.log(`UserCount:'${userCount}` )

        // Compare numberOfValues with userCount
    if (numberOfValues === userCount) {
        logToGlobalFile("The number of values matches the user count.")
        cy.log('The number of values matches the user count.');
    } else {
        logToGlobalFile("The number of values does not match the user count.")
        cy.log('The number of values does not match the user count.');
    }
    });
    }
  });



}

export function verify_shared_with_me(workflowname) {
        dashboard.openMenu();
        dashboard.clickSideMenuLink(data.sidelinks[2])
        cy.get(reportLocators.report.sharedwithme).eq(1).click()
        cy.get(reportLocators.report.workflowName).should('have.text', workflowname)
        cy.get(reportLocators.report.sharecount).should('be.be.visible')
        
}

export function verify_sharegroup(){
        dashboard.openMenu();
        dashboard.clickSideMenuLink(data.sidelinks[2]);
    
       
    
    
    
}

export function getCustomAttributes (){
        cy.get(reportLocators.report.certifiedBox).click()
        //cy.get(reportLocators.report.ZoOwner).type('Test')
        cy.get(reportLocators.report.businessOwner).type("hruser")
        cy.get('.user-list-cursor').contains('HRUSER USER <hruser@zedventures.com>').click()
        cy.wait(20000)


}

export function getRGCustomAttr(zo_desc, business_owner, comment ){
        logToGlobalFile("Clicking the 'certify button");
        cy.get(reportLocators.report.cerify_checkbox).click()

        logToGlobalFile("Clicking the 'Certify Custom' button");
        cy.get(reportLocators.report.certify_custom).click()

        // for Level 1
        logToGlobalFile("Selecting 'Level 1' for certification");
        cy.get(reportLocators.report.certify_level).eq(0).contains('Level 1').click()

        // for Level 2
         // cy.log("Selecting 'Level 2' for certification");
        //cy.get(reportLocators.report.certify_custom).click()
        //cy.get(reportLocators.report.certify_level).eq(1).contains('Level 2').click()
        //cy.get('.common-modal__content--input > .mat-form-field > .mat-form-field-wrapper > .mat-form-field-flex > .mat-form-field-infix').type('hruser')
        //cy.get('.search-userList > :nth-child(2) > span').contains('hruser <hruser@zenoptics.com>').click()

        // for Level 3
        //cy.get('.custom-style-for-mat-inputs').click()
        //cy.get("mat-option[role='option']").eq(2).contains('Level 3').click()
        //cy.get(':nth-child(3) > .new-user-container > app-user-search > .common-modal__content--input > .mat-form-field > .mat-form-field-wrapper > .mat-form-field-flex > .mat-form-field-infix').type('hruser')
        //cy.get('.search-userList > :nth-child(2) > span').contains('hruser <hruser@zenoptics.com>').click()

        // Save Button
        logToGlobalFile("Click on Save button");
        cy.get(reportLocators.report.certify_savebtn).click()
        
        logToGlobalFile(`Typing Zo Desc in the text area: ${zo_desc}`);
        cy.get(reportLocators.report.zo_dec).click({force:true})
        cy.get(reportLocators.report.zo_des_textarea).type(zo_desc)

        logToGlobalFile('Wait: 3000 milliseconds');
        cy.wait(3000)
        
        logToGlobalFile(`Typing Business Owner in the text area: ${business_owner}`);
        cy.get(reportLocators.report.bussiness_owner).click({force:true})
        cy.get(reportLocators.report.bussiness_owner_textarea).click().type(business_owner)
        cy.get(reportLocators.report.business_userlist).eq(1).click()

        logToGlobalFile(`Typing Commnets in the text area: ${comment}`);
        cy.get(reportLocators.report.comments).click().type(comment)

        cy.wait(5000)
        

}

export function getcustomdata() {
        let category
        let report_type
        let source_system
        let report_owner

        return cy.get(reportLocators.report.reportcategory).eq(3).invoke('text').then((text) => {
                const trimmedText = text.trim();
                category = `${trimmedText.replace('/', '')} `; 
                return cy.get(reportLocators.report.report_type_info).eq(1).invoke('text');
              }).then((text) => {
                report_type = ` ${text.trim()} `;
                return cy.get(reportLocators.report.report_type_info).eq(2).invoke('text');
              }).then((text) => {
                source_system = ` ${text.trim()} `;
                return cy.get(reportLocators.report.report_owner).invoke('text');
              }).then((text) => {
                report_owner = ` ${text.trim()} `;

                return { category, report_type, source_system, report_owner };
              });

}

export function verify_RGInfo(reportname, zo_dec, business_owner) {
        const description = ` ${zo_dec} `;
        const businessowner = ` ${business_owner} `;
        
        dashboard.openMenu()

        logToGlobalFile(`Click side menu link: ${data.sidelinks[1].Reports[0]}`);
        dashboard.clickSideMenuLink(data.sidelinks[1].Reports[0])

        logToGlobalFile(`Click side menu link: ${data.sidelinks[1].Reports[2]}`);
        dashboard.clickSideMenuLink(data.sidelinks[1].Reports[2])

        logToGlobalFile("Change sort to Newest First");
        cy.get(reportLocators.report.sort).click()
        cy.get(reportLocators.report.sort_items).eq(0).click()
        
        
        cy.get(reportLocators.report.thumbnail_card).eq(0).as('thumbnailCard');
        logToGlobalFile("Verify Certify Button");
        cy.get('@thumbnailCard').find(reportLocators.report.certify_icon).eq(0).should('exist')
        
        logToGlobalFile("Click on the information button");
        cy.get('@thumbnailCard').find(reportLocators.report.info_btn).eq(0).click({force:true})
        getcustomdata().then((data) => {
                logToGlobalFile(`Check info category: ${data.category}`)
                cy.get(reportLocators.report.info_category).should('have.text',data.category);
                logToGlobalFile(`Check info report type: ${data.report_type}`) 
                cy.get(reportLocators.report.info_report_type).should('have.text', data.report_type)
                logToGlobalFile(`Check info source system: ${data.source_system}`) 
                cy.get(reportLocators.report.info_source_system).should('have.text', data.source_system)
                logToGlobalFile(`Check info business owner: ${data.report_owner}`)
                cy.get(reportLocators.report.info_businessowner).eq(4).should('have.text', data.report_owner)
                logToGlobalFile(`Check info description: ${description}`)
                cy.get(reportLocators.report.info_description).invoke('text').should('eq', description);
                logToGlobalFile(`Check info business owner (eq. 5): ${businessowner}`)
                cy.get(reportLocators.report.info_businessowner).eq(5).invoke('text').should('eq', businessowner);
              });
        

}

export function verifyCat(){
        cy.get(reportLocators.report.thumbnail_card).eq(0).click()
        reportAbout()
        cy.get(reportLocators.report.report_category).invoke('text').then((category_path) => {
                const splitValues = category_path.split(" / ");
                console.log(splitValues);
              });

}


export function getmostlyviewedreport(){
        let initialViewCount

        cy.get(reportLocators.report.dashboard_div).eq(1).within(() => {
        
        // Capture the initial view count
        cy.get(reportLocators.report.viewcount).eq(0).then(($span) => {
                initialViewCount = $span.text().trim();
                cy.log(`Initial View Count: ${initialViewCount}`);
              });
            
              // Click on the favorite report card
              cy.get(reportLocators.report.selectmostlyviwedreport).eq(0).click();
            
              // Wait for a specified duration (you can adjust the duration as needed)
              cy.wait(60000); //wait for 3 mins
              cy.wait(2000)
              cy.get("span[class='ng-star-inserted']").eq(4).click({force:true})
              dashboard.clickHomeIcon()
              // Go back to the previous page
              //cy.go('back')
              //cy.get("div[class='rep-bar-report-new selected-report'] div[class='ng-star-inserted']").click()
             
              // Wait for a specified duration after going back (adjust as needed)
              cy.wait(5000);
      
              //reload
              //cy.reload()
            
              // Verify that the view count has been incremented by 1
              cy.get(reportLocators.report.viewcount).eq(0).then(($span) => {
                const updatedViewCount = $span.text().trim();
                cy.log(`Updated View Count: ${updatedViewCount}`);
                expect(Number(updatedViewCount)).to.equal(Number(initialViewCount) + 1);
              });
        
})
}

export function getmostlyusedreport(){
        let initialViewCount

        cy.get(reportLocators.report.dashboard_div).eq(2).within(() => {
        
        // Capture the initial view count
        cy.get(reportLocators.report.viewcount).eq(0).then(($span) => {
                initialViewCount = $span.text().trim();
                cy.log(`Initial View Count: ${initialViewCount}`);
              });
        dashboard.clickProfile()
        dashboard.logout()
        logins.loginUser2(Cypress.env(), Cypress.env('username1'),Cypress.env('password1234'))

        
      
})

}


export function captureViewCountAndVerifyIncrement_fav() {
        let initialViewCount;
      
        // Open the menu and navigate to the report
        dashboard.openMenu();
        dashboard.clickSideMenuLink(data.sidelinks[1].Reports[0]);
        dashboard.clickSideMenuLink(data.sidelinks[1].Reports[2]);
      
        // Capture the initial view count
        cy.get(reportLocators.report.viewcount).eq(0).then(($span) => {
          initialViewCount = $span.text().trim();
          cy.log(`Initial View Count: ${initialViewCount}`);
        });
      
        // Click on the favorite report card
        cy.get(reportLocators.report.favcard_report).eq(0).click();
      
        // Wait for a specified duration (you can adjust the duration as needed)
        cy.wait(240000); //wait for 3 mins
      
        // Go back to the previous page
        cy.go('back');
      
        // Wait for a specified duration after going back (adjust as needed)
        cy.wait(9000);

        //reload
        //cy.reload()
      
        // Verify that the view count has been incremented by 1
        cy.get(reportLocators.report.viewcount).eq(0).then(($span) => {
          const updatedViewCount = $span.text().trim();
          cy.log(`Updated View Count: ${updatedViewCount}`);
          expect(Number(updatedViewCount)).to.equal(Number(initialViewCount) + 1);
        });
}

export function captureViewCountAndVerifyIncrement_recently_view() {
        let initialViewCount;
      
        // Open the menu and navigate to the report
        dashboard.openMenu();
        dashboard.clickSideMenuLink(data.sidelinks[1].Reports[0]);
        dashboard.clickSideMenuLink(data.sidelinks[1].Reports[4]); //click Recently Viewed      
        // Capture the initial view count
        cy.get(reportLocators.report.viewcount).eq(0).then(($span) => {
          initialViewCount = $span.text().trim();
          cy.log(`Initial View Count: ${initialViewCount}`);
        });
      
        // Click on the favorite report card
        cy.get(reportLocators.report.favcard_report).eq(0).click();
      
        // Wait for a specified duration (you can adjust the duration as needed)
        cy.wait(240000); //wait for 3 mins
      
        // Go back to the previous page
        cy.go('back');
      
        // Wait for a specified duration after going back (adjust as needed)
        cy.wait(9000);

        //reload
        //cy.reload()
      
        // Verify that the view count has been incremented by 1
        cy.get(reportLocators.report.viewcount).eq(0).then(($span) => {
          const updatedViewCount = $span.text().trim();
          cy.log(`Updated View Count: ${updatedViewCount}`);
          expect(Number(updatedViewCount)).to.equal(Number(initialViewCount) + 1);
        });
}

export function getRandomReport_Dash(){
        let initialViewCount;

        cy.get(reportLocators.report.dashboard_div).eq(0).within(() => {
        //cy.get(reportLocators.report.selectmostlyviwedreport).eq(0).click()

        // Capture the initial view count
        cy.get(reportLocators.report.viewcount).eq(0).then(($span) => {
                initialViewCount = $span.text().trim();
                cy.log(`Initial View Count: ${initialViewCount}`);
              });
            
              // Click on the favorite report card
              cy.get(reportLocators.report.selectmostlyviwedreport).eq(0).click()
            
              // Wait for a specified duration (you can adjust the duration as needed)
              cy.wait(4000); //wait for 3 mins
            
              // Go back to the previous page
              cy.go('back')
              //cy.go('back')
            
              // Wait for a specified duration after going back (adjust as needed)
              cy.wait(9000);
      
              //reload
              //cy.reload()
            
              // Verify that the view count has been incremented by 1
              cy.get(reportLocators.report.viewcount).eq(0).then(($span) => {
                const updatedViewCount = $span.text().trim();
                cy.log(`Updated View Count: ${updatedViewCount}`);
                expect(Number(updatedViewCount)).to.equal(Number(initialViewCount) + 1);
              });

        
})
}

export function captureViewcount_List(){
        let initialViewCount;
      
        // Capture the initial view count
        cy.get(reportLocators.report.list_viewcount).eq(0).then(($span) => {
                initialViewCount = $span.text().trim();
                cy.log(`Initial View Count: ${initialViewCount}`);
              });
            
              // Click on the favorite report card
              cy.get(reportLocators.report.list_card).eq(0).click();
            
              // Wait for a specified duration (you can adjust the duration as needed)
              cy.wait(6000); //wait for 1 mins
            
              // Go back to the previous page
              cy.go('back');
            
              // Wait for a specified duration after going back (adjust as needed)
              cy.wait(9000);
      
              //reload
              //cy.reload()
            
              // Verify that the view count has been incremented by 1
              cy.get(reportLocators.report.list_viewcount).eq(0).then(($span) => {
                const updatedViewCount = $span.text().trim();
                cy.log(`Updated View Count: ${updatedViewCount}`);
                expect(Number(updatedViewCount)).to.equal(Number(initialViewCount) + 1);
              });
      }

      export function captureViewcount_Thumb(){
        let initialViewCount;
      
        // Capture the initial view count
        cy.get(reportLocators.report.viewcount).eq(0).then(($span) => {
                initialViewCount = $span.text().trim();
                cy.log(`Initial View Count: ${initialViewCount}`);
              });
            
              // Click on the favorite report card
              cy.get(reportLocators.report.favcard_report).eq(0).click();
            
              // Wait for a specified duration (you can adjust the duration as needed)
              cy.wait(4000); //wait for 1 mins
            
              // Go back to the previous page
              cy.go('back');
            
              // Wait for a specified duration after going back (adjust as needed)
              cy.wait(9000);
      
              //reload
              //cy.reload()
            
              // Verify that the view count has been incremented by 1
              cy.get(reportLocators.report.viewcount).eq(0).then(($span) => {
                const updatedViewCount = $span.text().trim();
                cy.log(`Updated View Count: ${updatedViewCount}`);
                expect(Number(updatedViewCount)).to.equal(Number(initialViewCount) + 1);
              });
      }

      export function captureViewcount_Grid(){
        let initialViewCount;
      
        // Capture the initial view count
        cy.get(reportLocators.report.grid_viewcount).eq(0).then(($span) => {
                initialViewCount = $span.text().trim();
                cy.log(`Initial View Count: ${initialViewCount}`);
              });
            
              // Click on the favorite report card
              cy.get(reportLocators.report.grid_card).eq(0).click();
            
              // Wait for a specified duration (you can adjust the duration as needed)
              cy.wait(6000); //wait for 1 mins
            
              // Go back to the previous page
              cy.go('back');
            
              // Wait for a specified duration after going back (adjust as needed)
              cy.wait(9000);
      
              //reload
              //cy.reload()
            
              // Verify that the view count has been incremented by 1
              cy.get(reportLocators.report.grid_viewcount).eq(0).then(($span) => {
                const updatedViewCount = $span.text().trim();
                cy.log(`Updated View Count: ${updatedViewCount}`);
                expect(Number(updatedViewCount)).to.equal(Number(initialViewCount) + 1);
              });
      }

export function getCategories() {
        cy.get(reportLocators.report.grid_categories).eq(0).click()
        cy.get(reportLocators.report.grid_categories).eq(0).click()
        cy.get(reportLocators.report.grid_categories).eq(0).click()
        

}

export function getReportTypes() {
   
        cy.get(reportLocators.report.report_types).eq(0).click()
}

export function getRandomReport_publish(){
        
        dashboard.openMenu();
        //dashboard.clickSideMenuLink(data.SIDELINKS_REPORTS.Reports)
        dashboard.clickSideMenuLink(data.sidelinks[1].Reports[0])//added code to avoid exception
        //cy.wait(50000) //added for srv15, its takimg too long to open myreports page
        cy.intercept('GET', '**/jaxrs/GetAllReports**').as('getAllReports')
        cy.get('.mat-drawer [class="mat-list-item-content"]').contains('My Reports').click()
        cy.wait('@getAllReports').then((interception) => {
          const response = interception.response.body;
          const reportName = response.reports[0].DISPLAY_VALUE;
          cy.wait(4000)
          setting.categorizedReportPage_new()
          cy.get(reportLocators.report.search_cat).eq(0).type(reportName).type('{enter}')
          cy.get("div[class='tree ng-star-inserted']").contains('#transaction').within(() => {
                cy.get(".mat-icon.notranslate.mat-icon-no-color").should('be.visible').should('be.enabled').then(($icon) => {
                  console.log('Mat Icon found:', $icon);
                }).click();
              });    
        })



}

export function openDashReport(){
    cy.get(reportLocators.report.dashreport).eq(0).click()
}

export function reportdetails(){
        cy.get(reportLocators.report.zo_dec).click({force:true})
        cy.get(reportLocators.report.zo_des_textarea).clear().type('Test Automation')

        cy.wait(3000)
 
        cy.get(reportLocators.report.bussiness_owner).click({force:true})
        cy.get(reportLocators.report.bussiness_owner_textarea).click().type('HRUSER')
        cy.get(reportLocators.report.business_userlist).eq(1).click()
        //cy.get(reportLocators.report.cerify_checkbox).click()
        //cy.get("div[aria-label]").should('be.visible')
        //cy.get("div[aria-label]").invoke('text').then((text) => {
          //      if (text.includes('Certified')) {
                    //cy.log('Report is Certified Successfully')
                    //cy.get(reportLocators.report.zo_dec).click({force:true})
                     //cy.get(reportLocators.report.zo_des_textarea).clear().type('Test Automation')

                       // cy.wait(3000)
 
                        //cy.get(reportLocators.report.bussiness_owner).click({force:true})
                        //cy.get(reportLocators.report.bussiness_owner_textarea).click().type('HRUSER')
                        //cy.get(reportLocators.report.business_userlist).eq(1).click()
                //} 
                //else {
                        //cy.get(reportLocators.report.cerify_checkbox).click()
                        //cy.get("div[aria-label]").should('be.visible')
                        //cy.log('Report is Certified Successfully')
                        //cy.get(reportLocators.report.zo_dec).click({force:true})
                     //cy.get(reportLocators.report.zo_des_textarea).clear().type('Test Automation')

                       // cy.wait(3000)
 
                       // cy.get(reportLocators.report.bussiness_owner).click({force:true})
                       // cy.get(reportLocators.report.bussiness_owner_textarea).click().type('HRUSER')
                       // cy.get(reportLocators.report.business_userlist).eq(1).click()

                //}
            //})
        //cy.get("div[aria-label='Report is Certified Successfully']").should('be.visible')

        


}
export function openDashReportinfo(){
        cy.get(reportLocators.report.dash_infobtn).eq(0).click({force:true})
}

export function openDashboardinfo_details(){
        //cy.get(reportLocators.report.certify_icon).eq(0).should('be.visible')
        cy.wait(1000)
        cy.get(reportLocators.report.dash_infobtn).eq(0).click({force:true})
        //cy.get(reportLocators.report.info_businessowner).should('be.visible')
        //cy.get(reportLocators.report.info_description).should('be.visible')
}

export function openDashboardinfo_details_off(){
        cy.get(reportLocators.report.certify_icon).eq(0).should('not.exists')
        cy.wait(1000)
        cy.get(reportLocators.report.dash_infobtn).eq(0).click({force:true})
        //cy.get(reportLocators.report.info_businessowner).should('not.exist')
        //cy.get(reportLocators.report.info_description).should('not.exist')
}

export function openTableauReport(reportType, reportname){
        dashboard.openMenu()
       cy.get(".mat-card.mat-focus-indicator.zo-main-reportlist-card.action-card._mat-animation-noopable.ng-star-inserted").contains(reportType).click()
       cy.get("mat-card").contains(reportname).click()
       cy.wait(9000)
    cy.get("span[class='ng-star-inserted']").eq(4).click({force:true})
    cy.get("span[class='label']").eq(0).should('exist')
    cy.get("span[class='label']").eq(1).should('exist')
    cy.get("span[class='label']").eq(2).should('exist')
    cy.get("span[class='label']").eq(3).should('exist') 
    cy.get("span[class='label']").eq(4).should('exist')
    cy.get("span[class='label']").eq(5).should('exist')
    cy.get("span[class='label']").eq(6).should('exist')
    cy.get("span[class='label']").eq(7).should('exist')

}
export function createWorkflow_user() {
        let userCount = 0;
        gobal.searchAndSelectFirstReport('Input Controls & Filter Demo-3010')
        cy.wait(2000)
        gobal.searchAndSelectFirstReport('do_not_use_5453_Geo Analysis Demo_25151261')
        cy.wait(2000)
        gobal.searchAndSelectFirstReport('Geo Analysis Demo-test3010')
        cy.wait(2000)
        logToGlobalFile("Clicking on save Button")
        cy.get(reportLocators.report.getMyWorkspaceSaveButton).click()
        logToGlobalFile("Clicking on next Button");
        cy.get(reportLocators.report.getMyWorkspaceNextButton).click()
        //logToGlobalFile(`Type ${workflowname} to save workflow`);
        cy.get(reportLocators.report.getWorkflowName).type('Test_WF_5')
        //logToGlobalFile(`Type ${workflowname}`);
        cy.get(reportLocators.report.getWorkflowDescription).type('Test_WF_5')
        //logToGlobalFile(`Type ${workflowname}`);
        cy.get(reportLocators.report.getWorkflowShareable).type('Test_WF_5')
        // Iterate over users
        

    for (const user of users) {
        //logToGlobalFile(`Clicked on the report search field ${user}`);
        cy.get(reportLocators.report.search).click({force:true}).type('testuser1');
        logToGlobalFile('Clicked on the first user in the user list');
        cy.get(reportLocators.report.userList).eq(0).click(); // Assuming the first user in the list
        //logToGlobalFile(` Type ${workflowname}`)
        userCount++
        cy.log(userCount)
    }
    
        logToGlobalFile("Click save button") 
        cy.get(reportLocators.report.saveWorkflowButton).click()
        logToGlobalFile('Verified that the workflow toaster notification exists and has the expected text');
        
        cy.get(reportLocators.report.workflowToasterNotification).should('exist').and('have.text', ' Workflow created successfully ')
        
        return userCount;
}


        














  
