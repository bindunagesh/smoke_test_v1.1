class setting {

    getThumbnailSettings() {
       return cy.get("#mat-tab-label-0-5 > .mat-tab-label-content")
      }
    getThumbnailSearch(){
        return cy.get('[placeholder="Search Reports"]')
      }

      getThumbnailcard(){
        return cy.get('[class="mat-row ng-star-inserted"]')
      }
      getThumbnailUpload(){
        return cy.get('tbody[role="rowgroup"] >tr :nth-child(8)>input')
      }
      getThumbnailUploadButton(){
        return cy.contains("Upload Thumbnail Images")
      }
      getThumbnailUploadedState(){
        return cy.get('.cdk-column-imageStatus > span')
      }

      getThumbnailActionColumn(){
        return cy.get('tbody[role="rowgroup"] >tr :nth-child(8)')
      }

      getThumbnailDeleteAction(){
        return cy.get('[svgicon="remove-manual-thumbnail"]')
      }
      getThumbnailDeleteButton(){
        return cy.get('tbody[role="rowgroup"] >tr :nth-child(8) > [svgicon="remove-manual-thumbnail"]')

      }
      getThumbnailDeleteConfirmation(){
        return cy.get('.zo-btn-delete')
        
      }



    }

  export default setting;
  