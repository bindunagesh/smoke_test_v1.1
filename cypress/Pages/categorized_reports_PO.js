export function selectCategory(categoryName){
    cy.get('.CR-tree-col-2  div.mat-tooltip-trigger').contains(categoryName).click()

}

export function selectcategorycheckbox(categoryName){
    cy.get('.CR-tree-col-2  div.mat-tooltip-trigger').contains(categoryName)
    .parent()
    .children().eq(0).click()
    .parent()
    .children().eq(3)
    .find('[svgicon="dot-menu"]').click({force:true})
    cy.get('.cdk-overlay-connected-position-bounding-box div.mat-menu-panel button').eq(10).click()

}