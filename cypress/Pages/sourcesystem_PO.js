
import * as sourceConnectionLocators from "../locators/sourcesystem_locators"
import { logToGlobalFile } from '../utilities/global_log'







export function createTableauConnection(sourceSystemName, connectionName, connectionParametersLength, tabDBConn, hostName, userName, password, apiLink,
    apiVersion, apiUsername, apiPassword, executableWorkbooks, populateBIDict, publishExtraction,
    metadataApi, metadataApiLink, contentUrl, disableThumbnailGeneration, loginType, thumbnail, thumbnailUserName, thumbnailPassword) {
    cy.get(sourceConnectionLocators.sourceConnectionTab.getAddNewSource).click()
    cy.get(sourceConnectionLocators.sourceConnectionTab.getConnectionType).click()
    cy.get(sourceConnectionLocators.sourceConnectionTab.getConnectionTypeDropdown).contains(sourceSystemName).click()
    cy.get(sourceConnectionLocators.sourceConnectionTab.getConnectionName).type(connectionName)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getConnectionParameters).should('have.length', connectionParametersLength)
    var array = [tabDBConn, hostName, userName, password, apiLink,
        apiVersion, apiUsername, apiPassword, executableWorkbooks, populateBIDict, publishExtraction,
        metadataApi, metadataApiLink, contentUrl, disableThumbnailGeneration, loginType];
    var i;
    for (let i = 0; i < array.length; i++) {
        cy.get(sourceConnectionLocators.sourceConnectionTab.getConnectionParameters).eq(i).clear().type(array[i]).wait(200);
    }
    // Thumbnail generation
    if (thumbnail === 'yes') {
        cy.get(sourceConnectionLocators.sourceConnectionTab.getThumbnailParametersYes).click() 
        cy.get(sourceConnectionLocators.sourceConnectionTab.getThumbnailGenerationUserName).type(thumbnailUserName) 
        cy.get(sourceConnectionLocators.sourceConnectionTab.getThumbnailGenerationPassword).type(thumbnailPassword) 
    }
    else {
        cy.get(sourceConnectionLocators.sourceConnectionTab.getThumbnailParameters).click()
    }
    cy.get(sourceConnectionLocators.sourceConnectionTab.getTestConnection).click()
    cy.get(sourceConnectionLocators.sourceConnectionTab.getToasterNotification).should('exist').and('have.text', ' Test Connection Successful ')
    cy.get(sourceConnectionLocators.sourceConnectionTab.getSaveConnection).click()
    cy.get(sourceConnectionLocators.sourceConnectionTab.getToasterNotification).should('exist').and('have.text', ' System Added Successfully ')
}


export function createPowerBIOnPremConnection(sourceSystemName, connectionName,connectionParametersLength, hostname, domainName, userName, password,
    reportServerDBHost, reportServerDBID, reportServerDBPassword, reportServerName, populateBIDict, publishExtraction, thumbnail, thumbnailUserName, thumbnailPassword, sucessfulTest, systemAdded) {
    cy.wait(6000)
    cy.visit(systemsourceinfo)
    cy.get(sourceConnectionLocators.sourceConnectionPowerBIOnPrem.getAddNewSource).click()
    cy.get(sourceConnectionLocators.sourceConnectionPowerBIOnPrem.getConnectionType).click()
    cy.get(sourceConnectionLocators.sourceConnectionPowerBIOnPrem.getConnectionTypeDropdown).contains(sourceSystemName).click()
    cy.get(sourceConnectionLocators.sourceConnectionPowerBIOnPrem.getConnectionName).type(connectionName)
    cy.get(sourceConnectionLocators.sourceConnectionPowerBIOnPrem.getConnectionParameters).should('have.length', connectionParametersLength)
    var array = [hostname, domainName, userName, password, reportServerDBHost, reportServerDBID, reportServerDBPassword, reportServerName, populateBIDict, publishExtraction];
    var i;
    for (i = 0; i < array.length; i++) {
        cy.get(sourceConnectionLocators.sourceConnectionPowerBIOnPrem.getConnectionParameters).eq(i).clear().type(array[i])
    }
    //Thumbnail generation
    if (thumbnail === 'yes') {
        cy.get(sourceConnectionLocators.sourceConnectionPowerBIOnPrem.getThumbnailParameters).click()
        cy.get(sourceConnectionLocators.sourceConnectionPowerBIOnPrem.getThumbnailGenerationUserName).type(thumbnailUserName)
        cy.get(sourceConnectionLocators.sourceConnectionPowerBIOnPrem.getThumbnailGenerationPassword).type(thumbnailPassword)
    }
    else {
        cy.get(sourceConnectionLocators.sourceConnectionPowerBIOnPrem.getThumbnailParametersYes).click()
    }
    cy.get(sourceConnectionLocators.sourceConnectionPowerBIOnPrem.getTestConnection).contains('Test Connection').click()
    cy.get(sourceConnectionLocators.sourceConnectionTab.getToasterNotification).should('exist').and('have.text', ' Test Connection Successful ')
    cy.get(sourceConnectionLocators.sourceConnectionPowerBIOnPrem.getSaveConnection).contains('Save').click()
    cy.get(sourceConnectionLocators.sourceConnectionTab.getToasterNotification).should('exist').and('have.text', ' System Added Successfully ')
}


export function createSSRSConnection(sourceSystemName,connectionName,hostName,userId,password,databaseName,
    publishBIDict,publishAfterExtraction,loginType,useRestApi,domainName,apiUrl,apiUserName,apiPassword) {
    cy.wait(6000)
    cy.visit(systemsourceinfo)
    cy.get(sourceConnectionLocators.sourceConnectionSSRS.getAddNewSource).click()
    cy.get(sourceConnectionLocators.sourceConnectionSSRS.getConnectionType).click()
    cy.get(sourceConnectionLocators.sourceConnectionSSRS.getConnectionTypeDropdown).contains(sourceSystemName).click()
    cy.get(sourceConnectionLocators.sourceConnectionSSRS.getConnectionName).type(connectionName)
    cy.get(sourceConnectionLocators.sourceConnectionSSRS.getHostName).clear().click()
    cy.get(sourceConnectionLocators.sourceConnectionSSRS.getHostName).type(hostName)
    cy.get(sourceConnectionLocators.sourceConnectionSSRS.getUserName).clear().type(userId)
    cy.get(sourceConnectionLocators.sourceConnectionSSRS.getUserPassword).clear().type(password)
    cy.get(sourceConnectionLocators.sourceConnectionSSRS.getDatabaseName).clear().type(databaseName)
    cy.get(sourceConnectionLocators.sourceConnectionSSRS.getPublishBIDictionary).eq(4).clear().type(publishBIDict)
    cy.get(sourceConnectionLocators.sourceConnectionSSRS.getPublishAfterExtraction).eq(5).clear().type(publishAfterExtraction)
    cy.get(sourceConnectionLocators.sourceConnectionSSRS.getLoginType).eq(6).clear().type(loginType)
    cy.get(sourceConnectionLocators.sourceConnectionSSRS.getUseRestApi).eq(7).clear().type(useRestApi)
    cy.get(sourceConnectionLocators.sourceConnectionSSRS.getDomainName).eq(8).clear().type(domainName)
    cy.get(sourceConnectionLocators.sourceConnectionSSRS.getApiUrl).eq(9).clear().type(apiUrl)
    cy.get(sourceConnectionLocators.sourceConnectionSSRS.getApiUserName).eq(10).clear().type(apiUserName)
    cy.get(sourceConnectionLocators.sourceConnectionSSRS.getApiPassword).eq(11).clear().type(apiPassword)
    cy.get(sourceConnectionLocators.sourceConnectionSSRS.getThumbnailParametersYes).click()
    cy.get(sourceConnectionLocators.sourceConnectionSSRS.getTestConnection).contains('Test Connection').click()
    cy.get(sourceConnectionLocators.sourceConnectionSSRS.getSaveConnection).contains('Save').click()


    }


export function searchTableauConnection(connectionName) {
   
    cy.get(sourceConnectionLocators.sourceConnectionTab.getSearchConnection).click()
    cy.get(sourceConnectionLocators.sourceConnectionTab.getTypeConnectionNameInSearch).type(connectionName)
}




export function schedulerun(connectionName) {
    logToGlobalFile('Clicking on the search connection button.');
    cy.get(sourceConnectionLocators.sourceConnectionTab.getSearchConnection).click()
    logToGlobalFile(` Typing connection name '${connectionName}' in the search input.`);
    cy.get(sourceConnectionLocators.sourceConnectionTab.getTypeConnectionNameInSearch).type(connectionName)
    logToGlobalFile(` Navigating to the third table on the page`);
    cy.get(sourceConnectionLocators.sourceConnectionTab.getTable).eq(3)
    logToGlobalFile('Clicking on the "Schedule Job" button for the first item in the table');
    cy.get(sourceConnectionLocators.sourceConnectionTab.getTableauRun).eq(0).should('have.attr', 'mattooltip', 'Schedule Job').click({ force: true })
    logToGlobalFile('Clicking on the submit button in the user form');
    cy.get('.zo-userForm-button [type="submit"]').click()
    logToGlobalFile('Verifying the existence and text of the toaster notification');
    cy.get(sourceConnectionLocators.sourceConnectionTab.getToasterNotification)
    .should('exist')
    .and('have.text', ' Schedule created successfully ');
    //cy.task('taskLogToTerminalReport',`${formattedDate} Verifying the text in the scheduler period in the mat-sidenav`);
    //cy.get('#mat-select-value-17').invoke(text).should('have.text', ' Daily ')
}
export function searchTableauRunComplete(connectionName) {
    logToGlobalFile('Clicking on the search connection icon');
    cy.get(sourceConnectionLocators.sourceConnectionTab.getSearchConnection).click()
    logToGlobalFile(` Typing connection name in the search field: ${connectionName}`);
    cy.get(sourceConnectionLocators.sourceConnectionTab.getTypeConnectionNameInSearch).type(connectionName)
    logToGlobalFile('Selecting the 4th table in the search results');
    cy.get(sourceConnectionLocators.sourceConnectionTab.getTable).eq(3)
    logToGlobalFile('Initiating Tableau run by clicking on the Run Complete button');
    cy.get(sourceConnectionLocators.sourceConnectionTab.getTableauRun).eq(1).should('have.attr', 'mattooltip', 'Run Complete').click({ force: true })
    logToGlobalFile('Verifying toaster notification for run initiation');
    cy.get(sourceConnectionLocators.sourceConnectionTab.getToasterNotification)
    .should('exist')
    .and('have.text', ` ${connectionName} run complete initiated `);
    logToGlobalFile('Waiting for Tableau run to complete (timeout: 800000 milliseconds)');
    cy.get(sourceConnectionLocators.sourceConnectionTab.getToasterNotification, { timeout: 800000 })
    .should('exist')
    .and('have.text', ` ${connectionName} run complete is successfully completed `);
}

export function searchTableauRun(connectionName) {
    cy.get(sourceConnectionLocators.sourceConnectionTab.getSearchConnection).click()
    cy.get(sourceConnectionLocators.sourceConnectionTab.getTypeConnectionNameInSearch).type(connectionName)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getTable).eq(3)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getTableauRun).eq(1).should('have.attr', 'mattooltip', 'Run').click({ force: true })
    cy.get(sourceConnectionLocators.sourceConnectionTab.getToasterNotification)
    .should('exist')
    .and('have.text', ` ${connectionName} run complete initiated `);
    cy.get(sourceConnectionLocators.sourceConnectionTab.getToasterNotification, { timeout: 800000 })
    .should('exist')
    .and('have.text', ` ${connectionName} run complete is successfully completed `);
}


export function deleteTableauConnection() {
  
    cy.get(sourceConnectionLocators.sourceConnectionTab.getConnectionNameCheckbox).click({multiple:true})//added multiple true
    cy.wait(2000)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getDeleteConnection).click()
    cy.get(sourceConnectionLocators.sourceConnectionTab.getDeleteButton).click()
}






export function createTableauV10Connection(sourceSystemName, connectionName,  tabDBConn, hostName, userName, password, apiLink,
    apiVersion, apiUsername, apiPassword, executableWorkbooks, populateBIDict, publishExtraction,
    metadataApi, metadataApiLink, contentUrl, disableThumbnailGeneration, loginType, thumbnail, thumbnailUserName, thumbnailPassword) {
    cy.get(sourceConnectionLocators.sourceConnectionTab.getAddNewSource).click()
    cy.wait(500)
    cy.get('.mat-drawer>.mat-drawer-inner-container form.zo-user-form').click
    cy.get(sourceConnectionLocators.sourceConnectionTab.getConnectionType).click({force: true})
    cy.get(sourceConnectionLocators.sourceConnectionTab.getConnectionTypeDropdown).contains(sourceSystemName).click()
    cy.get(sourceConnectionLocators.sourceConnectionTab.getConnectionName).clear().type(connectionName)
    cy.get(sourceConnectionLocators.sourceConnectionTab.gettableaudbConn).clear().type(tabDBConn)
    cy.get(sourceConnectionLocators.sourceConnectionTab.gettableauserver).clear().type(hostName)
    cy.get(sourceConnectionLocators.sourceConnectionTab.gettableauusername).clear().type(userName)
    cy.get(sourceConnectionLocators.sourceConnectionTab.gettableaupassword).clear().type(password)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getapilink).clear().type(apiLink)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getapiversion).clear().type(apiVersion)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getapiusername).clear().type(apiUsername)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getapipassword).clear().type(apiPassword)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getexecutableworkbooks).clear().type(executableWorkbooks)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getpopulatebidictionary).clear().type(populateBIDict)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getpublishafterextraction).clear().type(publishExtraction)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getmetadataapi).clear().type(metadataApi)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getmetadataapilink).clear().type(metadataApiLink)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getcontentUrl).clear().type(contentUrl)



    
    // // Thumbnail generation
    // if (thumbnail === 'yes') {
    //     cy.get(sourceConnectionLocators.sourceConnectionTab.getThumbnailParametersYes).click()
    //     cy.get(sourceConnectionLocators.sourceConnectionTab.getThumbnailGenerationUserName).type(thumbnailUserName)
    //     cy.get(sourceConnectionLocators.sourceConnectionTab.getThumbnailGenerationPassword).type(thumbnailPassword)
    // }
    // else {
    //     cy.get(sourceConnectionLocators.sourceConnectionTab.getThumbnailParameters).click()
    // }
    cy.get(sourceConnectionLocators.sourceConnectionTab.getTestConnection).click()
    cy.get(sourceConnectionLocators.sourceConnectionTab.getToasterNotification).should('exist').and('have.text', ' Test Connection Successful ')
    cy.get(sourceConnectionLocators.sourceConnectionTab.getSaveConnection).click()
    cy.get(sourceConnectionLocators.sourceConnectionTab.getToasterNotification).should('exist').and('have.text', ' System Added Successfully ')
}

export function createTestConnection(sourceSystemName, connectionName,  tabDBConn, hostName, userName, password, apiLink,
    apiVersion, apiUsername, apiPassword, executableWorkbooks, populateBIDict, publishExtraction,
    metadataApi, metadataApiLink, contentUrl, disableThumbnailGeneration, loginType, thumbnail, thumbnailUserName, thumbnailPassword) {
    cy.get(sourceConnectionLocators.sourceConnectionTab.getAddNewSource).click()
    cy.wait(500)
    cy.get('.mat-drawer>.mat-drawer-inner-container form.zo-user-form').click
    cy.get(sourceConnectionLocators.sourceConnectionTab.getConnectionType).click({force: true})
    cy.get(sourceConnectionLocators.sourceConnectionTab.getConnectionTypeDropdown).contains(sourceSystemName).click()
    cy.get(sourceConnectionLocators.sourceConnectionTab.getConnectionName).clear().type(connectionName)
    cy.get(sourceConnectionLocators.sourceConnectionTab.gettableaudbConn).clear().type(tabDBConn)
    cy.get(sourceConnectionLocators.sourceConnectionTab.gettableauserver).clear().type(hostName)
    cy.get(sourceConnectionLocators.sourceConnectionTab.gettableauusername).clear().type(userName)
    cy.get(sourceConnectionLocators.sourceConnectionTab.gettableaupassword).clear().type(password)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getapilink).clear().type(apiLink)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getapiversion).clear().type(apiVersion)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getapiusername).clear().type(apiUsername)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getapipassword).clear().type(apiPassword)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getexecutableworkbooks).clear().type(executableWorkbooks)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getpopulatebidictionary).clear().type(populateBIDict)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getpublishafterextraction).clear().type(publishExtraction)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getmetadataapi).clear().type(metadataApi)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getmetadataapilink).clear().type(metadataApiLink)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getcontentUrl).clear().type(contentUrl)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getdisablethumbnailgeneration).clear().type(disableThumbnailGeneration)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getlogin_type).clear().type(loginType)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getThumbnailPara).click({force:true})
    cy.get("span[class='mat-radio-outer-circle']").eq(4).click({force:true})
    cy.get(sourceConnectionLocators.sourceConnectionTab.getThumbnailGenerationUserName).type(thumbnailUserName)
    cy.get(sourceConnectionLocators.sourceConnectionTab.getThumbnailGenerationPassword).type(thumbnailPassword)

    
    // // Thumbnail generation
    // if (thumbnail === 'yes') {
    //     cy.get(sourceConnectionLocators.sourceConnectionTab.getThumbnailParametersYes).click()
    //     cy.get(sourceConnectionLocators.sourceConnectionTab.getThumbnailGenerationUserName).type(thumbnailUserName)
    //     cy.get(sourceConnectionLocators.sourceConnectionTab.getThumbnailGenerationPassword).type(thumbnailPassword)
    // }
    // else {
    //     cy.get(sourceConnectionLocators.sourceConnectionTab.getThumbnailParameters).click()
    // }
    cy.get(sourceConnectionLocators.sourceConnectionTab.getTestConnection).click()
    cy.get(sourceConnectionLocators.sourceConnectionTab.getToasterNotification).should('exist').and('have.text', ' Test Connection Successful ')
    cy.get(sourceConnectionLocators.sourceConnectionTab.getSaveConnection).click()
    cy.get(sourceConnectionLocators.sourceConnectionTab.getToasterNotification).should('exist').and('have.text', ' System Added Successfully ')
}
