import * as login from "../locators/login_locators"


function getConfiguration() {
    const zenTestType = Cypress.env().zenTestType;
    if(zenTestType == "WinTrial") {
        return Cypress.env().winTrail;
    }

    else if(zenTestType == "qa2") {
        return Cypress.env().qa2;
    }

    else if(zenTestType == "trial") {
        return Cypress.env().trial;
    }
    else if(zenTestType == "srv14") {
        return Cypress.env().srv14;
    }

}


function getCredentials(config){
    const zenTestUserType = Cypress.env().zenTestUserType;
    if(zenTestUserType === "admin") {
        return config.admin;
    }
}

export function loginUser(env) {
    const config = getConfiguration();
    const credential = getCredentials(config);
    cy.visit(config.url);
    cy.get(login.usn_input).type(credential.username);
    cy.get(login.pwd_input).type(credential.password);
    cy.get(login.submit_btn).click();
}


// export function loginUser(env) {
//     cy.visit(Cypress.env(env));
  
// }



