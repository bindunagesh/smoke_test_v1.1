import * as reportStatisticsLocators from "../locators/reportStatistics_locators";
let graphname = 'Usage by Source'
let reportheader = 'URL'
let reportname = 'TestURL'



  export function verifyreportStatistics() {
    cy.wait(4000)
    for (let i = 0; i < 4; i++) {
      cy.get(reportStatisticsLocators.reportStats.reportstatgraph).eq(i).should('be.visible');
    }
    cy.get(reportStatisticsLocators.reportStats.overallradiobtn).eq(0).click({force:true})
    cy.wait(4000)
    for (let i = 0; i < 4; i++) {
      cy.get(reportStatisticsLocators.reportStats.reportstatgraph).eq(i).should('be.visible');
    }
      //cy.get(reportStatisticsLocators.reportStats.reportstatgraph).eq(1).should('be.visible')
    
  }

  export function toggleBackendServerButton(){
    cy.wait(4000)
    cy.get(reportStatisticsLocators.reportStats.toggleBackendServer).click()
    cy.wait(5000)

  }

  export function verifybackendstatistics() {
    cy.wait(4000)
    for (let i = 0; i < 4; i++) {
      cy.get(reportStatisticsLocators.reportStats.reportstatgraph).eq(i).should('be.visible');
    }
    cy.get(reportStatisticsLocators.reportStats.overallradiobtn).eq(1).click({force:true})
    cy.wait(4000)
    for (let i = 0; i < 4; i++) {
      cy.get(reportStatisticsLocators.reportStats.reportstatgraph).eq(i).should('be.visible');
    }
     
  }

  export function verifyreportusage() {
    //let reportname = 'ProductManufacturing'
    cy.wait(4000)
    cy.get(reportStatisticsLocators.reportStats.overallradiobtn).eq(1).click({force:true})
    cy.get(reportStatisticsLocators.reportStats.reportstatgraph).eq(2).should('be.visible')
    //cy.get(reportStatisticsLocators.reportStats.bargraph).eq(23).should('be.visible').click()
    //cy.get(reportStatisticsLocators.reportStats.reportname).contains(reportname)
    //cy.get(reportStatisticsLocators.reportStats.closebtn).click()
    
  }

  export function searchreport() {
    cy.get(reportStatisticsLocators.reportStats.searchbox).should('be.visible').type(reportname)
    cy.get(reportStatisticsLocators.reportStats.openreport).eq(0).click()
  }

 
  export function verify_report() {
    let clickFlag = 0;
  
    cy.get(reportStatisticsLocators.reportStats.graph)
      .should('be.visible')
      .each($graph => {
        cy.wrap($graph)
          .invoke('text')
          .then(graphText => {
            cy.log(`Graph: ${graphText}`);
  
            if (graphText.includes(graphname) && clickFlag === 0) {
              clickFlag = 1;
              clickBarsAndCheckHeader($graph);

            }
          });
      });
  }
  
  function clickBarsAndCheckHeader($graph) {
    
    cy.wrap($graph)
      .find(reportStatisticsLocators.reportStats.bargraph)
      .each($bar => {
        cy.wrap($bar).click({ force: true });
  
        cy.get(reportStatisticsLocators.reportStats.reportheader)
          .should('be.visible')
          .invoke('text')
          .then(reportHeaderText => {
            if (reportHeaderText.includes(reportheader)) {
              cy.get(reportStatisticsLocators.reportStats.reportname)
                .contains(reportname).should('exist');
  
              cy.log('Condition met. Stopping execution.');
              
              //cy.pause()
              cy.then(() => {
                cy.get(reportStatisticsLocators.reportStats.closebtn).click();
              });
            } else {
              cy.get(reportStatisticsLocators.reportStats.closebtn).click();
            }
          });
      });
  }

  
  


  