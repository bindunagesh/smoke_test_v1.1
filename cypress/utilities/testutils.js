const fs = require('fs');
const logFilePath = 'C:\Users\Ankitha\cypressautomation\logs\error.log'; // Update with the actual path

export function logError(error, testCaseName) {
  const formattedDate = new Date().toISOString().replace(/:/g, '-').split('.')[0];
  const errorMessage = error ? error.stack || error.message || error : 'Test case skipped';
  const errorDetails = `${formattedDate} Error in ${testCaseName}: ${errorMessage}\n`;

  // Log to the console for debugging
  cy.log(errorDetails);

  // Append error details to a file
  fs.appendFileSync(logFilePath, errorDetails);

  // Log to the terminal report using cy.task
  cy.task('taskLogToTerminalReport', errorDetails);
}
