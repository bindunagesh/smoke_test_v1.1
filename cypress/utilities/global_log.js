import fs from 'fs';
import path from 'path';
import { addContext } from 'mochawesome/addContext';

export function logScreenshots() {
  const screenshotsFolder = 'cypress/screenshots'; // Adjust the folder path based on your Cypress configuration
  //const testTitle = Cypress.mocha.currentTest.fullTitle();
  const screenshotPath = path.join(screenshotsFolder, Cypress.spec.name, `${testTitle}.png`);

  if (fs.existsSync(screenshotPath)) {
    addContext({ test: 'Screenshot', value: screenshotPath });
  }
}

const logToGlobalFile = (message) => {
  const now = new Date();
  const options = { year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit', hour12: false};
  const formattedDate = now.toLocaleString('en-US', options).replace(/[\/,:\s]/g, '-');
  //const formattedDate = now.toISOString().replace(/:/g, '-').replace('T', ' ').split('.')[0];
  cy.task('taskLogToTerminalReport', `${formattedDate} ${message}`);
};

export { logToGlobalFile};