
const biDictionaryLocators ={
  
    table:{
     
        actionColoumn : 'div.zo-table tbody>tr.mat-row>td:nth-child(9) mat-icon',
        searchFirstPage : '[data-mat-icon-name="Search_Global"]',
        searchSecondPage : '.search-block>input[data-placeholder="Search"]',
        authorizedUserButton : 'div.zo-table tbody>tr.mat-row>td:nth-child(5) mat-icon[mattooltip="Authorized Users"]',
        PaginationDropDown : 'section.zo-user-ids .mat-paginator-page-size mat-form-field',
        PaginationDropDownOptions : '.cdk-overlay-pane mat-option',
        AuthorizedUserList: '.search-userList>.ng-star-inserted'
    },


    editRecord:{
        approvedCheckbox : 'mat-sidenav mat-checkbox',
        updateButton : 'mat-sidenav button[type="submit"]'

    }








}
module.exports = biDictionaryLocators




