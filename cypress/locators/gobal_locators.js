const gobalLocators ={
  
    gobalSearch:{
     
        search : '.searchbar',
        searchCard : '.zo-searched-reports .zo-list-data',
        searchFirstReport : ':nth-child(1) > .actionIcon > .data-title > .zo-report-name',
        searchCardInfoIcon : ':nth-child(1) > .actionIcon div.data-end div.zo-lock-container>mat-icon',
        searchCardThumbnail : '.thumbnail-card-shadow',
        searchCardThumbnailFooter : '.thumbnail-card-shadow div.last-used-certify-cont',
        searchCardThumbnailFooterFavIcon : '.thumbnail-card-shadow div.last-used-certify-cont div mat-icon[data-mat-icon-name="favorite-icon"]',
        advanceSearch : '.title-end.ng-star-inserted',
        biglossary_search: "div[class='search-block']",
        select_item: "div[class='mat-tooltip-trigger field-names cursor-pointer is-active ng-star-inserted']",
        report_dropdown: "span[class='mat-select-min-line ng-tns-c137-3 ng-star-inserted']",
        click_reports: '.mat-option-text'
        },

    reportViews:{
        

        viewSelection : '[class="zo-icon-section"]',
        gridView : '[class="zo-icon-section"]>mat-icon[mattooltip="Grid View"]',
        gridContainer:'section .zo-report-grid-container',
        gridreport:'.ng-star-inserted>section.zo-report-grid-container>mat-card',
        gridReporNAme:'.zo-report-grid-container span.mat-tooltip-trigger',
        listView : '[class="zo-icon-section"]>mat-icon[mattooltip="List View"]',
        listContainer:'section .zo-report-list-container',
        listViewReportName: '.zo-report-list-container .zo-report-list-details span.zo-displayname',
        thumbnailView : '[class="zo-icon-section"]>mat-icon[mattooltip="Thumbnail View"]',
        thumbnailContainer:'section .paginated-content',
        thumbnailContainer2:'.thumbnail-reports-container',
        thumbnailReportName:'.zo-thumbnails-view-container>.thumbnail-reports-container .fav-display-value',
        thumbnailReportName2:'.thumbnail-reports-container .fav-display-value',
        multiviewbutton: '.workspace-action-icons mat-icon[mattooltip="Multi View"]',
        multiviewenablebutton: '.zo-reportview-container button:nth-child(2)',
        splitview:'div [class="reports report-flex report-flex-for-multiview"]',
        splitviewreports:'div [class="reports report-flex report-flex-for-multiview"]>section',
        singleview: 'div [class="reports report-flex-for-multiview"]',
        singleviewreports:'div [class="reports report-flex-for-multiview"]>section',
        singleviewbutton:'mat-radio-button[value="SingleView"]',
        multiviewRefreshSchedule:'button.zo-schedule-button',
        multiviewFrequencyBox:'[name="RestartSchedule"]',
        multiviewSelectFrequency:'[class="mat-option-text"]',
        multiViewFrequencyButton:'button.broadcast-notification__footer--save-btn',
        multiViewFrequencyTime:'div.auto-refresh-content .auto-refresh__time',
        workspaceReportName:'.reports-container .rep-bar-reportContainer .ng-star-inserted .tag-wrap-new>span',
        toastNotification:'#toast-container>div>div',
        thumbnail_card: "div[class='thumbnail-card ng-star-inserted']",
        fav_section: "section[class='paginated-content ng-star-inserted']"
        

        

},
quick_search:
{
    view: "mat-icon[class='mat-icon notranslate mat-tooltip-trigger zo-icon-star mat-icon-no-color']",
    rating: "mat-icon[svgicon='star-icon']",
    info: "mat-icon[svgicon='info_active']",
    allReports: "p[class='headeropt ng-star-inserted']",
    search_msg: "span[class='title-start ng-star-inserted']",
    source: "div[class='card-body'] p:nth-child(2)",
    allsource: "p[class='zo-group-name ng-star-inserted']",
    categories: ".zo-active-search",
    allcategories: "p[class='zo-group-name ng-star-inserted']"
},
advance_Search:{
    view: "mat-icon[svgicon='views-icon']"
}


}
module.exports = gobalLocators