const sourceConnectionLocators ={
  
    sourceConnectionTab:{
     
        getAddNewSource : '.zo-users-header-right > [mattooltip="Add New Source"]',
        getConnectionType : '.mat-form-field-infix mat-select[formcontrolname="SourceOptions"]',
        getConnectionTypeDropdown : 'mat-option > .mat-option-text',
        getConnectionName: '[formcontrolname="systemName"]',
        getConnectionParameters : '[class="zo-connection-params ng-star-inserted"]>div',
        getThumbnailParameters : '#mat-radio-3 > .mat-radio-label > .mat-radio-container > .mat-radio-inner-circle',
        getThumbnailParametersYes : '#mat-radio-2 > .mat-radio-label > .mat-radio-container > .mat-radio-inner-circle',
        getThumbnailGenerationUserName : '#mat-input-17',
        getThumbnailGenerationPassword :  '#mat-input-18',
        getlogin_type: '#login_type',
        getThumbnailPara: '[id="mat-radio-5-input"]',
        getTestConnection : '.zo-btn-test',
        getSaveConnection : '.zo-btn-success > .mat-button-wrapper',
        getSearchConnection : '.zo-users-header-right > .zo-MT2 > [mattooltip="Search Source(s)"]',
        getTypeConnectionNameInSearch : '#mat-input-0',
        getConnectionNameCheckbox : 'tbody .mat-checkbox-layout',
        getDeleteConnection : '.zo-users-header-right > [mattooltip="Select row(s) for Deletion"]',
        getDeleteButton : '.zo-btn-delete > .mat-button-wrapper',
        getToasterNotification : '#toast-container>div>div',
        getSourceSystemAction: '[class="source-system-info-ie"]>div>mat-icon',
        getTableauRun: '[class="source-system-info-ie"]>div>mat-icon',
        getTable : 'tbody>tr>td',
        gettableaudbConn : '[id="tableau.dbConn"]',
        gettableauserver:'[id="tableau.server"]',
        gettableauusername:'[id="tableau.username"]',
        gettableaupassword: '[id="tableau.password"]',
        getapilink:'[id="api.link"]',
        getapiversion:'[id="api.version"]',
        getapiusername:'[id="api.username"]',
        getapipassword:'[id="api.password"]',
        getexecutableworkbooks:'[id="executable.workbooks"]',
        getpopulatebidictionary:'[id="populate.bi.dictionary"]',
        getpublishafterextraction:'[id="publishafterextraction"]',
        getmetadataapi:'[id="metadata.api"]',
        getmetadataapilink:'[id="metadata.api.link"]',
        getcontentUrl:'[id="contentUrl"]',
        getdisablethumbnailgeneration: '[id="disable_api_generated_thumbnail"]'



        





    },
    sourceConnectionPowerBIOnPrem:{
     
        getAddNewSource : '.zo-users-header-right > [mattooltip="Add New Source"]',
        getConnectionType : '.mat-select-placeholder',
        getConnectionTypeDropdown : 'mat-option > .mat-option-text',
        getConnectionName: '[formcontrolname="systemName"]',
        getConnectionParameters : '[class="zo-connection-params ng-star-inserted"]>div',
        getThumbnailRadioButton : '[class="zo-connection-params ng-star-inserted"]>div>div>mat-radio-group>mat-radio-button',
        getThumbnailParametersYes : '#mat-radio-3 > .mat-radio-label > .mat-radio-container > .mat-radio-inner-circle',
        getThumbnailParameters : '#mat-radio-2 > .mat-radio-label > .mat-radio-container > .mat-radio-inner-circle',
        getThumbnailGenerationUserName : '#mat-input-11',
        getThumbnailGenerationPassword :  '#mat-input-12',
        getTestConnection : '.zo-btn-test',
        getSaveConnection : '.zo-btn-success > .mat-button-wrapper',
        getToasterNotification : '#toast-container>div>div'
    },

    sourceConnectionBOBJ:{
     
        getAddNewSource : '.zo-users-header-right > [mattooltip="Add New Source"]',
        getConnectionType : '.mat-select-placeholder',
        getConnectionTypeDropdown : 'mat-option > .mat-option-text',
        getConnectionName: '[formcontrolname="systemName"]',
        getConnectionParameters : '[class="zo-connection-params ng-star-inserted"]>div',
        getHostName : '#bobj.host',
        getCMSPort : '#bobj.port',
        getUserId : '#bobj.user',
        getPassword : '#bobj.passwd',
        getStatsDBType : '#statsdbtype',
        getStatsUrl : '#statsdburl',
    },

    sourceConnectionSSRS:{
        getAddNewSource : '.zo-users-header-right > [mattooltip="Add New Source"]',
        getConnectionType : '.mat-select-placeholder',
        getConnectionTypeDropdown : 'mat-option > .mat-option-text',
        getConnectionName: '[formcontrolname="systemName"]',
        getConnectionParameters : '[class="zo-connection-params ng-star-inserted"]>div',
        getHostName : '#hostName',
        getUserName : '#userName',
        getUserPassword : '#passwd',
        getDatabaseName : '#dbname',
        getPublishBIDictionary : '[formarrayname="ConnectionParams"]',
        getPublishAfterExtraction : '[formarrayname="ConnectionParams"]',
        getLoginType : '[formarrayname="ConnectionParams"]',
        getUseRestApi : '[formarrayname="ConnectionParams"]',
        getDomainName : '[formarrayname="ConnectionParams"]',
        getApiUrl : '[formarrayname="ConnectionParams"]',
        getApiUserName : '[formarrayname="ConnectionParams"]',
        getApiPassword : '[formarrayname="ConnectionParams"]',
        getThumbnailParametersYes : '#mat-radio-3 > .mat-radio-label > .mat-radio-container > .mat-radio-inner-circle',
        getThumbnailParameters : '#mat-radio-2 > .mat-radio-label > .mat-radio-container > .mat-radio-inner-circle',
        getTestConnection : '.zo-btn-test',
        getSaveConnection : '.zo-btn-success > .mat-button-wrapper',
        
    }

}
module.exports =sourceConnectionLocators