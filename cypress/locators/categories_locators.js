const categoriesLocators ={
  
    mainPage:{
     
        mainCategoryName: '.ng-star-inserted>.zo-grid-container span.mat-tooltip-trigger',
        
        },

        subCategoriesPage:{
        subCategoryName:'.ng-star-inserted .zo-fixed-height',
        sortMenu : '.zo-sort-section>div.zo-sort',
        sortSelection : '.zo-sort-section div.sort-container div.sort-list-item',
        selectedSortText :'.zo-sort-section>.zo-sort>.zo-sort-text'
       
        
}


}
module.exports = categoriesLocators