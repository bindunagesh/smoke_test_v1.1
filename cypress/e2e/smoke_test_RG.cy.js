// <reference types="Cypress" />


import { randomData } from  '../Pages/fakerData'
import data from "../fixtures/sanityData.json"
import * as sourceConnection from "../fixtures/sourceConnection.json"
import * as source from "../pages/sourcesystem_PO"  
import navbarlinks from "../fixtures/leftnavbarlinks.json";
import navbarreportlinks from "../fixtures/leftnavbarreportlinks.json";
import dashboardLinks from "../fixtures/dashboard_links.json";
import * as dashboard from "../Pages/dashboard_PO";
import * as favorite from "../pages/favorite_PO"
import * as gobal from "../Pages/gobal_PO";
import * as setting from "../Pages/setting_PO";
import * as report from "../Pages/report_PO";
import * as biDictionary from "../Pages/biDictonary_PO";
import { checkEmail } from "../utilities/email";
import { checkEmailSubscribe } from "../utilities/email"
import * as reportType from "../Pages/reportType_PO";
import * as categories from "../Pages/categories_PO";
import * as biGlossary from '../Pages/biGlossary_PO'
import * as logins from'../Pages/logins_PO'
import * as categorizedReportPage from '../Pages/categorized_reports_PO'
import * as ldap from "../pages/ldapSetting_PO"
import * as ldapConfiguaration from "../fixtures/ldapConfiguration.json"
import * as reportRating from "../fixtures/report.json"
import rgdata from "../fixtures/RG_Data.json"


describe("Smoke Suite RG Click Certification", () => {
  beforeEach(() => {
    
    logins.loginUser2(Cypress.env(),data.login.username,data.login.password);
       
    })

  it.only("Add Custom Attribute using RG Certification", ()=>{
    cy.task('taskLogToTerminalReport','TC-001_Add Custom Attribute using RG Certification');
    setting.changetoRG()
    gobal.searchAndSelectReport(rgdata.report_details.report_name)
    cy.task('taskLogToTerminalReport', 'Wait: 5000 milliseconds');
    cy.wait(5000)
    
    report.toggleReportsFavoritesOn()
    report.reportAbout()
    report.getcustomdata()
    report.getRGCustomAttr(rgdata.report_details.zo_description, rgdata.report_details.business_owner, rgdata.report_details.comment)
    dashboard.clickHomeIcon()
    report.verify_RGInfo(rgdata.report_details.report_name, rgdata.report_details.zo_description, rgdata.report_details.business_owner_det)
    
  })
  

  it('Verify the report in Dashboard', ()=> {
    //setting.changetoRG()
    dashboard.verifyFavoriteInDashboard('1 KPI Dashboard ')

  })

  it('Verify Report in My Report', ()=>{
    //setting.changetoRG()
    dashboard.openMenu()
    dashboard.clickSideMenuLink(data.sidelinks[1].Reports[0])
    dashboard.clickSideMenuLink(data.sidelinks[1].Reports[1])
    dashboard.verifyInMyReport()

    cy.screenshot()
  })

  it ('Verify the Report in Favourite',()=>{
    //setting.changetoRG()
    dashboard.openMenu()
    dashboard.verifyReportInFav()

    cy.screenshot()
  })

  it('Verify the Report in Whats New',()=>{
    //setting.changetoRG()
    dashboard.openMenu()
    dashboard.verifyReportInWhatsNew()

    cy.screenshot()
    
  })

  it('Verify the Report in Recently Viewed',()=>{
    //setting.changetoRG()
    dashboard.openMenu()
    dashboard.verifyReportInRecentlyViewed()

    cy.screenshot()
  })

  it.skip('Verify in Category',()=>{
    //setting.changetoRG()
    //dashboard.verifyInCategory()
    dashboard.openMenu()
    dashboard.clickSideMenuLink(data.sidelinks[1].Reports[0])
    dashboard.clickSideMenuLink(data.sidelinks[1].Reports[1])
    report.verifyCat()

  })
  
  it('Verify from RM',()=>{
    //setting.changetoRG()
    dashboard.verifyFromRMForRG()
    cy.screenshot()

  })

  //
  it('Categorised Report Page',()=>{
    //setting.changetoRG()
    dashboard.openMenu()
    setting.categorizedReportPage()
    cy.get('.search-input').type('2023 Info').type('{enter}')
  })

  it('Verify Report from Search',()=>{
    //setting.changetoRG()
    gobal.gobalSearch('2023 Info')
    cy.get('.zo-lock-container').click()
    cy.screenshot()
    

    cy.wait(5000)
  })

  it("Verify all the 3 views (List, Grid, Thumbnail) in My Reports, Favorites, What's New, Recently Viewed", () => {
    //setting.changetoRG()
    gobal.verifytheviews(data.sidelinks[1].Reports[1]);
    gobal.verifytheviews(data.sidelinks[1].Reports[2]);
    gobal.verifytheviews(data.sidelinks[1].Reports[3]);
    gobal.verifytheviews(data.sidelinks[1].Reports[4]);
  })

})

