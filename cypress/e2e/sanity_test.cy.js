/// <reference types="Cypress" />
import {
  LOGIN_USERNAME,
  LOGIN_PASSWORD,
  SEARCH_REPORT,
  VERIFY_REPORT,
  MANUAL_THUMBNAILS_SEARCH_REPORT,
  MANUAL_THUMBNAILS_VERIFY_REPORT,
  MANUAL_THUMBNAILS_UPLOAD_PATH,
  AUTOMATION_THUMBNAILS_SEARCH_REPORT,
  AUTOMATION_THUMBNAILS_VERIFY_REPORT,
  AUTOMATION_THUMBNAILS_QUERY,
  MULTI_VIEW_NUMBER_OF_REPORTS,
  FAVORITE_REPORT_NAME,
  FAVORITE_REPORT_NAME1,
  FAVORITE_REPORT_NAME2,
  EMAIL_REPORT_SEARCH_AND_VERIFY_REPORT,
  EMAIL_REPORT_EMAIL_REPORT,
  EMAIL_REPORT_VERIFY_IN_EMAIL,
  BIDICTIONARY_FIELD_NAME,
  BIDICTIONARY_REPORT_NAME,
  BIDICTIONARY_EXPECTED_USER_COUNT,
  SIDELINKS_REPORTS,
  CHANGE_COMPANY_LOGO_UPLOAD_PATH,
  CHANGE_COMPANY_LOGO_FILENAME,
  SIDELINKS_DASHBOARD,
  SIDELINKS_WORKFLOW,
  SIDELINKS_CATEGORIES,
  SIDELINKS_BI_GLOSSARY,
  SIDELINKS_REPORT_TYPES,
  SIDELINKS_NETWORK_DRIVE,
  SIDELINKS_STEWARD_MANAGEMENT,
  SIDELINKS_REPORT_STATISTICS,
  SIDELINKS_ADMIN_SETTINGS,
  SUBSCRIBE_REPORT,
  COMMENT_REPORT_NAME,
  REPORT_USERID,
  REPORT_FIRSTLASTNAME,
  REPORT_TYPE_PAGE,
  REPORT_TYPE_CONNECTOR_TYPE,
  REPORT_TYPE_SORT_ORDER,
  REPORT_TYPE_REPORT_NAME,
  CATERGORIZED_REPORT_PAGE,
  CATERGORIZED_REPORT_MAIN_CATEGORY,
  CATERGORIZED_REPORT_SUBCATEGORY,
  CATERGORIZED_REPORT_SORT_ORDER,
  CATERGORIZED_REPORT_REPORT_NAME,

} from '../Pages/constants';
 
import * as sourceConnection from "../fixtures/sourceConnection.json"
import * as source from "../pages/sourcesystem_PO"  
import sideMenu from "../fixtures/sidelinks.json";
import navbarlinks from "../fixtures/leftnavbarlinks.json";
import navbarreportlinks from "../fixtures/leftnavbarreportlinks.json";
import dashboardLinks from "../fixtures/dashboard_links.json";
import * as login from "../pages/login_PO";
import * as dashboard from "../Pages/dashboard_PO";
import * as favorite from "../pages/favorite_PO"
import * as gobal from "../Pages/gobal_PO";
import * as setting from "../Pages/setting_PO";
import * as report from "../Pages/report_PO";
import * as biDictionary from "../Pages/biDictonary_PO";
import { checkEmail } from "../utilities/email";
import { checkEmailSubscribe } from "../utilities/email"
import * as reportType from "../Pages/reportType_PO";
import * as categories from "../Pages/categories_PO";
import * as biGlossary from '../Pages/biGlossary_PO'

describe('Tableau Data and Source Connection Test', () => {
  beforeEach(() => {
    login.loginUser("winTrail", "admin");
  }); 
  it("TC-00001_Verify creating a new Tableau Connection", () => {
    dashboard.openMenu()
    setting.openSourceConnectionDetails()

    source.createTableauConnection(
    
      sourceConnection.Tableau.systemName,
      sourceConnection.Tableau.connectionName,
      sourceConnection.Tableau.connectionParametersLength, 
      sourceConnection.Tableau.tabDBConn, 
      sourceConnection.Tableau.hostName, 
      sourceConnection.Tableau.userName, 
      sourceConnection.Tableau.password, 
      sourceConnection.Tableau.apiLink,
      sourceConnection.Tableau.apiVersion, 
      sourceConnection.Tableau.apiUsername, 
      sourceConnection.Tableau.apiPassword, 
      sourceConnection.Tableau.executableWorkbooks, 
      sourceConnection.Tableau.populateBIDict, 
      sourceConnection.Tableau.publishExtraction,
      sourceConnection.Tableau.metadataApi, 
      sourceConnection.Tableau.metadataApiLink, 
      sourceConnection.Tableau.contentUrl, 
      sourceConnection.Tableau.disableThumbnailGeneration, 
      sourceConnection.Tableau.loginType,
      sourceConnection.Tableau.thumbnail,
      sourceConnection.Tableau.thumbnailUserName,
      sourceConnection.Tableau.thumbnailPassword,
    )
  });
  
  it("TC-0001_Run Complete Tableau Connection", () => {
    dashboard.openMenu()
    setting.openSourceConnectionDetails()
    source.searchTableauRunComplete(sourceConnection.Tableau.connectionName)
  });
});
describe("Sainty Suite", () => {
  beforeEach(() => {
    login.loginUser("winTrail", "admin");
  });
 
  it.only("TC-001_Verify Links in the Side Panel are displayed properly (ZEN2022-TC-43)",{ tags: ["@smoke", "@sanity", "@regression"] },() => {
      dashboard.openMenu();
      dashboard.verifysideMenu(sideMenu);
    }
  );
  it.only("TC-002_1 Verify clicking on all the links on left nav bar expect Reports (ZEN2022-TC-44)", () => {
    dashboard.openMenu();
    dashboard.verifySideMenuLinks(navbarlinks);
  });
  it.only("TC-002_2 Verify clicking Reports and its sub links (ZEN2022-TC-44)", () => {
    dashboard.openMenu();
    dashboard.clickSideMenuLink(SIDELINKS_REPORTS.Reports);
    cy.log(SIDELINKS_REPORTS.Reports);
    dashboard.verifySideMenuLinks(navbarreportlinks);
  });
  it.only("TC-003 Verify performing search for a Report from Search bar (ZEN2022-TC-45)",{ tags: "@search" },() => {
      gobal.gobalSearch(SEARCH_REPORT);
      gobal.gobalSearchResult(VERIFY_REPORT);
    });
  it.only("TC-004_1 Verify all the 3 views (List, Grid, Thumbnail) in My Reports, Favorites, What's New, Recently Viewed", () => {
    gobal.verifytheviews(SIDELINKS_REPORTS.MyReports);
    gobal.verifytheviews(SIDELINKS_REPORTS.Favorites);
    gobal.verifytheviews(SIDELINKS_REPORTS.WhatsNew);
    gobal.verifytheviews(SIDELINKS_REPORTS.RecentlyViewed);
  });
  it.only("TC-005 Verify manual upload thumbnail generation", () => {
    dashboard.openMenu();
    setting.openThumbnailSettingTab();
    setting.thumbnailSearchReport(MANUAL_THUMBNAILS_SEARCH_REPORT);
    setting.thumbnailVerifySearch(MANUAL_THUMBNAILS_VERIFY_REPORT);
    setting.UploadThumbnail(MANUAL_THUMBNAILS_UPLOAD_PATH, { force: true });
  });
it.skip("TC-006 Verify Generate Thumbnail through automation and verify DB", () => {
    dashboard.openMenu();
    setting.openThumbnailSettingTab();
    setting.thumbnailSearchReport(AUTOMATION_THUMBNAILS_SEARCH_REPORT);
    setting.thumbnailVerifySearch(AUTOMATION_THUMBNAILS_VERIFY_REPORT);
    setting.deletethumbnailGenerateNew();
    cy.sqlServer(AUTOMATION_THUMBNAILS_QUERY).then((result) => {
      const job_status = result[0];
      expect(job_status).to.contain("waiting");
    });
  })
it.only('TC-010 Verify if the fields in BI Dictionary are showing report using this field and authorized user.', () => {
    dashboard.openMenu()
    setting.openBiDictionary()
    biDictionary.verifyAuthorizedUsersInBiDi(BIDICTIONARY_FIELD_NAME,BIDICTIONARY_REPORT_NAME,BIDICTIONARY_EXPECTED_USER_COUNT)
})
it.only("TC-011 Verify if the fields in BI Glossary are showing report using this field", () => {
    dashboard.openMenu();
    dashboard.clickSideMenuLink('BI Glossary');
    biGlossary.searchField('(555)XXX-XXXX"')
    biGlossary.resultReportTable('Agent Analysis')
    //need to add the data--note to self
   
  })
it.only('TC-13 Mark some reports as favorites and see if they show up on dashboard', () => {
    var i; 
    var a =[FAVORITE_REPORT_NAME,FAVORITE_REPORT_NAME1,FAVORITE_REPORT_NAME2]
    for(i=0;i<a.length;i++){
        gobal.searchAndSelectFirstReport(a[i])
        report.toggleReportFavoritesOn(i)
        dashboard.verifyFavoriteInDashboard(a[i] + " ")
        dashboard.clickFavoritesCarouselLink()
        favorite.verifyInFavoritesPage(a[i])
    }
})  
it.only('TC-015 Subscribe to a report and add public and private comments and see if the subscribed user gets the email notification on public comment', () => {   
        gobal.searchAndSelectFirstReport(SUBSCRIBE_REPORT)  
        report.toggleReportSubscribeOn()
        dashboard.clickProfile()
        dashboard.logout()
        login.loginUser('winTrail', 'userTwo')
        gobal.searchAndSelectFirstReport(SUBSCRIBE_REPORT)  
        report.reportPublicComment("Adding a public comment")
        checkEmailSubscribe('New Comment posted on your subscribed report "' + SUBSCRIBE_REPORT + '"')
})

it.only('TC-016 Launch the private comment from notification', () => {   
     gobal.searchAndSelectFirstReport(COMMENT_REPORT_NAME) 
     report.reportPrivateComment(REPORT_USERID,'testing private comment')
     dashboard.clickProfile()
     dashboard.logout()
     login.loginUser('winTrail', 'userTwo')
     dashboard.clickProfile()
     dashboard.clickNotifications()
     dashboard.clickNotificationPrivateComment() 
     report.verifyPrivateComment(REPORT_FIRSTLASTNAME,'testing private comment')
})
  
it.only('TC-019 Verfiy the multiview functionality', () => {
      dashboard.openMenu()
        setting.openMyReports()
        gobal.verifyMultiView(MULTI_VIEW_NUMBER_OF_REPORTS)

  })
it.only('TC-020 Verify favoriting a report', () => {
    gobal.searchAndSelectFirstReport(FAVORITE_REPORT_NAME)
    report.toggleReportsFavoritesOn()
    
})
it.only('TC-022 Email report link and verify if it can be sent to the people added in recipients field', () => {
        gobal.searchAndSelectFirstReport(EMAIL_REPORT_SEARCH_AND_VERIFY_REPORT)
        report.sendReportInEmail (EMAIL_REPORT_EMAIL_REPORT)
        checkEmail(EMAIL_REPORT_VERIFY_IN_EMAIL)
        
  }) 
  
it.skip("TC-026 Verify Report types page", () => {
    dashboard.openMenu();
    dashboard.clickSideMenuLink(REPORT_TYPE_PAGE);
    reportType.ConnectorType(REPORT_TYPE_CONNECTOR_TYPE);
    reportType.sortOrder(REPORT_TYPE_SORT_ORDER);
    gobal.viewReportIndifferentViews(REPORT_TYPE_REPORT_NAME);
  });
it.skip("TC-028 Verify Categorized Reports page", () => {
    dashboard.openMenu();
    dashboard.clickSideMenuLink(CATERGORIZED_REPORT_PAGE);
    categories.mainCategory(CATERGORIZED_REPORT_MAIN_CATEGORY);
    reportType.sortOrder(CATERGORIZED_REPORT_SORT_ORDER);
    categories.subCategory(CATERGORIZED_REPORT_SUBCATEGORY);
    reportType.sortOrder(CATERGORIZED_REPORT_SORT_ORDER);
    gobal.viewReportIndifferentViewsCategoriesPage(CATERGORIZED_REPORT_REPORT_NAME);
  });
  

  it.only('TC-031 Verify the favorited reports are displayed as "Favorites" in the search result', () => {
       gobal.verifyFavIconInSearch(FAVORITE_REPORT_NAME)
  })

  
  
 
 
  

})
describe('Tableau Data and Source Connection Test', () => {
  beforeEach(() => {
    login.loginUser("qa2", "admin");
  }); 
  it('Data Cleanup', () => {
    source.searchTableauConnection(sourceConnection.Tableau.connectionName)
    source.deleteTableauConnection()  
  });
});
