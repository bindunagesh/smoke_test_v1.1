/// <reference types="Cypress" />


import { randomData } from  '../Pages/fakerData'
import data from "../fixtures/sanityData.json"
import * as sourceConnection from "../fixtures/sourceConnection.json"
import * as source from "../pages/sourcesystem_PO"  
import navbarlinks from "../fixtures/leftnavbarlinks.json";
import navbarreportlinks from "../fixtures/leftnavbarreportlinks.json";
import dashboardLinks from "../fixtures/dashboard_links.json";
import * as dashboard from "../Pages/dashboard_PO";
import * as favorite from "../pages/favorite_PO"
import * as gobal from "../Pages/gobal_PO";
import * as setting from "../Pages/setting_PO";
import * as report from "../Pages/report_PO";
import * as biDictionary from "../Pages/biDictonary_PO";
import { checkEmail } from "../utilities/email";
import { checkEmailSubscribe } from "../utilities/email"
import * as reportType from "../Pages/reportType_PO";
import * as categories from "../Pages/categories_PO";
import * as biGlossary from '../Pages/biGlossary_PO'
import * as logins from'../Pages/logins_PO'
import * as categorizedReportPage from '../Pages/categorized_reports_PO'
import * as ldap from "../pages/ldapSetting_PO"
import * as ldapConfiguaration from "../fixtures/ldapConfiguration.json"
import * as reportRating from "../fixtures/report.json"
 
  
describe("Smoke Suite", () => {
  beforeEach(() => {
  
    logins.loginUser2(Cypress.env(),data.login.username,data.login.password);
    
   
  });
  it("TC-00001_Verify creating a new Tableau Connection", () => {
    dashboard.openMenu()
    setting.openSourceConnectionDetails()
     
    source.createTableauV10Connection(
    
      sourceConnection.Tableau.systemName,
      sourceConnection.Tableau.connectionName,
      sourceConnection.Tableau.tabDBConn, 
      sourceConnection.Tableau.hostName, 
      sourceConnection.Tableau.userName, 
      sourceConnection.Tableau.password, 
      sourceConnection.Tableau.apiLink,
      sourceConnection.Tableau.apiVersion, 
      sourceConnection.Tableau.apiUsername, 
      sourceConnection.Tableau.apiPassword, 
      sourceConnection.Tableau.executableWorkbooks, 
      sourceConnection.Tableau.populateBIDict, 
      sourceConnection.Tableau.publishExtraction,
      sourceConnection.Tableau.metadataApi, 
      sourceConnection.Tableau.metadataApiLink, 
      sourceConnection.Tableau.contentUrl, 
      sourceConnection.Tableau.disableThumbnailGeneration, 
      sourceConnection.Tableau.loginType,
      sourceConnection.Tableau.thumbnail,
      sourceConnection.Tableau.thumbnailUserName,
      sourceConnection.Tableau.thumbnailPassword,
    )
  });
  it('Add report type for Tableau', () => {
    dashboard.openMenu()
    setting.openReportTypes()
    setting.searchconnection()
    setting.editReportType()
    setting.reportUrl('Smoke Test Tableau','http://tableau-2021.zenoptics.com/','http://13.52.168.148/')
  });
  it("TC-0001_Run Complete Tableau Connection", () => {
    dashboard.openMenu()
    setting.openSourceConnectionDetails()
    source.searchTableauRunComplete(sourceConnection.Tableau.connectionName)
  });
  it("TC-0001_Run  Tableau Connection", () => {
    dashboard.openMenu()
    setting.openSourceConnectionDetails()
    source.searchTableauRun(sourceConnection.Tableau.connectionName)
  });
  it("Add category to whatsNew", () => {
    dashboard.openMenu();
    setting.categorizedReportPage()
   categorizedReportPage.selectcategorycheckbox(data.categorizedReportPage.categoryName)
    
  });
  it("TC-001_Verify Links in the Side Panel are displayed properly (ZEN2022-TC-43)",{ tags: ["@smoke", "@sanity", "@regression"] },() => {
      dashboard.openMenu();
      dashboard.verifysideMenu(data.sidelinksText);
    }
  );
  it("TC-002_1 Verify clicking on all the links on left nav bar expect Reports (ZEN2022-TC-44)", () => {
    dashboard.openMenu();
    dashboard.verifySideMenuLinks(navbarlinks);
  });
  it.only("TC-002_2 Verify clicking Reports and its sub links (ZEN2022-TC-44)", () => {
    dashboard.openMenu();
    dashboard.clickSideMenuLink(data.sidelinksText[1]);
    dashboard.verifySideMenuLinks(navbarreportlinks);
  });
  it("TC-003 Verify performing search for a Report from Search bar (ZEN2022-TC-45)",{ tags: "@search" },() => {
      gobal.gobalSearch(data.searchReports.searchReport);
      gobal.gobalSearchResult(data.searchReports.verifyReport);
    });
  it("TC-004_1 Verify all the 3 views (List, Grid, Thumbnail) in My Reports, Favorites, What's New, Recently Viewed", () => {
    gobal.verifytheviews(data.sidelinks[1].Reports[1]);
    gobal.verifytheviews(data.sidelinks[1].Reports[2]);
    gobal.verifytheviews(data.sidelinks[1].Reports[3]);
    gobal.verifytheviews(data.sidelinks[1].Reports[4]);
  });
it("TC-005 Verify manual upload thumbnail generation", () => {
    dashboard.openMenu();
    setting.openThumbnailSettingTab();
    setting.thumbnailSearchReport(data.manualThumbnails.searchReport);
    setting.thumbnailVerifySearch(data.manualThumbnails.verifyReport);
    setting.UploadThumbnail(data.changeCompanyLogo.uploadPath2, { force: true });
  });

it('TC-010 Verify if the fields in BI Dictionary are showing report using this field and authorized user.', () => {
    dashboard.openMenu()
    setting.openBiDictionary()
    biDictionary.verifyAuthorizedUsersInBiDi(data.biDictionary.fieldName,data.biDictionary.reportName,data.data.biDictionary.expectedUserCount)
})
it("TC-011 Verify if the fields in BI Glossary are showing report using this field", () => {
    dashboard.openMenu();
    setting.openBiDictionary()
    biDictionary.searchBiDictinaryField(data.biDictionary.fieldName)
    biDictionary.approvefield()
    dashboard.openMenu()
    dashboard.clickSideMenuLink(data.sidelinks[4]);
    biGlossary.searchField(data.biDictionary.fieldName)
    biGlossary.searchresult(data.biDictionary.fieldName)
  
    
   
  }) 
it('TC-015 Subscribe to a report and add public and private comments and see if the subscribed user gets the email notification on public comment', () => {   
        gobal.searchAndSelectFirstReport('UPN_URL_Report_1')  
        report.toggleReportSubscribeOn()
        dashboard.clickProfile()
        dashboard.logout()
        logins.loginUser2(Cypress.env(),'aamir',data.login.password)
        gobal.searchAndSelectFirstReport('UPN_URL_Report_1')  
        report.reportPublicComment("Adding a public comment")
        checkEmailSubscribe('New Comment posted on your subscribed report "' + 'UPN_URL_Report_1' + '"')
})
it('TC-016 Launch the private comment from notification', () => {   
     gobal.searchAndSelectFirstReport('UPN_URL_Report_1') 
     report.reportPrivateComment(data.user.userid,'testing private comment')
     dashboard.clickProfile()
     dashboard.logout()
     logins.loginUser2(Cypress.env(),'aamir',data.login.password)
     dashboard.clickProfile()
     dashboard.clickNotifications()
     dashboard.clickNotificationPrivateComment() 
     report.verifyPrivateComment(data.user.firstLastName,'testing private comment')
})
it('TC-019 Verfiy the multiview functionality', () => {
      dashboard.openMenu()
        setting.openMyReports()
        gobal.verifyMultiView(data.multiView.numberOfReports,2)

  })
it('TC-020 Verify favoriting a report', () => {
    gobal.searchAndSelectFirstReport(data.favoriteReport.reportName)
    report.toggleReportsFavoritesOn()
    
})
it('TC-022 Email report link and verify if it can be sent to the people added in recipients field', () => {
        gobal.searchAndSelectFirstReport(data.emailReport["search&VerifyReport"])
        report.sendReportInEmail (data.emailReport.verifyInEmail)
        checkEmail(data.emailReport.verifyInEmail)
        
  }) 
it("TC-026 Verify Report types page", () => {
    dashboard.openMenu();
    dashboard.clickSideMenuLink(data.reportType.page);
    dashboard.openMenu();
    reportType.ConnectorType(data.reportType.ConnectorType);
    reportType.sortOrder(data.reportType.sortOrder);
  });
it("TC-028 Verify Categorized Reports page", () => {
    dashboard.openMenu();
    setting.categorizedReportPage()
  
    categorizedReportPage.selectCategory(data.categorizedReportPage.categoryName)
  });
it('TC-031 Verify the favorited reports are displayed as "Favorites" in the search result', () => {
       gobal.verifyFavIconInSearch(data.favoriteReport.reportName)
  })

it(' Verify LDAP Connection', () => {
    dashboard.openMenu();
    setting.openLdapConfigurationTab()
    ldap.ldapWithOneUrl(data.ladpConfiguration.ldapServerUrl1)
    ldap.ldapBindDN(data.ladpConfiguration.ldapbinddn)
    ldap.ldapPassword(data.ladpConfiguration.ldappassword)
    ldap.ldapUserBase(data.ladpConfiguration.ldapuserbase)
    ldap.ldapAttributes(data.ladpConfiguration.ldapattributes)
    ldap.updateButton()

})

it('Verify Report Rating', () => {
  gobal.searchAndSelectFirstReport(" Sanjosehouses")       
  report.reportRating(reportRating)
});
it('Verify extractor scheduling functionality', () => {Jo
    dashboard.openMenu()
    setting.openSourceConnectionDetails()
    source.schedulerun(sourceConnection.Tableau.connectionName)
  
  });

it('Switching between classic and modern dashboard', () => {
    dashboard.clickProfile()
    dashboard.dashboardSetting()
    dashboard.toggleDashboardStyle()
   

  });


  it('Verify pinning/unpinning categories from categories page', () => {
    dashboard.clickProfile();
    dashboard.dashboardSetting();
    dashboard.toggleModernDashboard();
    dashboard.pinUnpinCatsFromCatPage()
 
  })
//in progress
  it('Verify pinning/unpinning categories from dashboard page', () => {
    dashboard.clickProfile();
    dashboard.dashboardSetting();
    dashboard.toggleModernDashboard();
    cy.intercept('GET', '**/analytics-catalog/categories**').as('getCategories');
    cy.get('.simple-page-scroller .icon-content-title').contains('Categories').click();
    cy.wait('@getCategories').then((interception) => {
    const response = interception.response.body;
    const categoryName = response.categories[0].categoryName;
    cy.get('#Home').click()
    cy.get('.categorys-card').contains(categoryName)
    .find('[data-mat-icon-name="ellipsis-icon"]').click()
    cy.get('.pin-workflow-dropdown>a').click()
 
  })
  })
  it('Verify new workflow created and delete successfully', () => {   
      report.getRandomReport() 
      report.createWorkflow('Automating WF','Aamir')
      report.deleteWorkflow('Automating WF')  
  });


  it('Verify Creating a Newly created User and verify accessing the application and dashboard', () => {
    const { email, firstName, lastName,password} = randomData();
    dashboard.openMenu()
    setting.openUsersettings()
    setting.createActiveUser(firstName,password,lastName,email)
    dashboard.clickProfile()
    dashboard.logout()
    logins.loginUser2(Cypress.env(),firstName,password)
    dashboard.openMenu()
    
  });
  it('Verify the Single view with Multiple reports open', () => {
    dashboard.openMenu()
    setting.openMyReports()
    gobal.verifySingleView(data.multiView.numberOfReports,2)
    
  });

  it('Verify the Split view with Multiple reports open', () => {
    dashboard.openMenu()
    setting.openMyReports()
    gobal.verifySplitView(data.multiView.numberOfReports,2)
    
  });

  it('Verify scheduling auto-refresh options in Single View', () => {
    dashboard.openMenu()
    setting.openMyReports()
    gobal.verifySingleViewSchedule(data.multiView.numberOfReports,2)
    
  

    
  });

  it('Verify scheduling auto-refresh options in split View', () => {
    dashboard.openMenu()
    setting.openMyReports()
    gobal.verifySplitViewSchedule(data.multiView.numberOfReports,2)
    
  

    
  });

  it('Tableau Connector Delete', () => {
    dashboard.openMenu()
    setting.openSourceConnectionDetails()
    source.searchTableauConnection(sourceConnection.Tableau.connectionName)
    source.deleteTableauConnection()
   
  });
})
