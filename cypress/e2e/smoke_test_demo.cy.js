/// <reference types="Cypress" />

import { randomData } from  '../Pages/fakerData'
import data from "../fixtures/sanityData.json"
import * as sourceConnection from "../fixtures/sourceConnection.json"
import * as source from "../pages/sourcesystem_PO"  
import navbarlinks from "../fixtures/leftnavbarlinks.json";
import navbarreportlinks from "../fixtures/leftnavbarreportlinks.json";
import dashboardLinks from "../fixtures/dashboard_links.json";
import * as dashboard from "../Pages/dashboard_PO";
import * as favorite from "../pages/favorite_PO"
import * as gobal from "../Pages/gobal_PO";
import * as setting from "../Pages/setting_PO";
import * as report from "../Pages/report_PO";
import * as biDictionary from "../Pages/biDictonary_PO";
import { checkEmail } from "../utilities/email";
import { checkEmailSubscribe } from "../utilities/email"
import * as reportType from "../Pages/reportType_PO";
import * as categories from "../Pages/categories_PO";
import * as biGlossary from '../Pages/biGlossary_PO'
import * as logins from'../Pages/logins_PO'
import * as categorizedReportPage from '../Pages/categorized_reports_PO'
import * as ldap from "../pages/ldapSetting_PO"
import * as ldapConfiguaration from "../fixtures/ldapConfiguration.json"
import * as reportRating from "../fixtures/report.json"
import * as reportStatistics from "../Pages/reportStatistics_PO"
import { createWorkflow_multipleuser } from '../Pages/report_PO'
import { logToGlobalFile, logScreenshots } from '../utilities/global_log'




  
describe("Smoke Suite", () => {
  beforeEach(() => {
    logToGlobalFile('Executing beforeEach hook');
    logins.loginUser2(Cypress.env(),Cypress.env('adminUserName'),Cypress.env('adminPassword'));
    
   
  });
  
  it("TC-001_Verify Links in the Side Panel are displayed properly (ZEN2022-TC-43)",{ tags: ["@smoke", "@sanity", "@regression"] },() => {
    logToGlobalFile("TC-001_Verify Links in the Side Panel are displayed properly (ZEN2022-TC-43)")
    dashboard.openMenu();
    dashboard.verifysideMenu(data.sidelinksText);
    cy.screenshot('Pass-test-001')
  });
  

it("TC-004 Verify all the 3 views (List, Grid, Thumbnail) in My Reports, Favorites, What's New, Recently Viewed", () => {
  logToGlobalFile("TC-004 Verify all the 3 views (List, Grid, Thumbnail) in My Reports, Favorites, What's New, Recently Viewed")
  gobal.verifytheviews(data.sidelinks[1].Reports[1]);
  gobal.verifytheviews(data.sidelinks[1].Reports[2]);
  //gobal.verifytheviews(data.sidelinks[1].Reports[3]);
  gobal.verifytheviews(data.sidelinks[1].Reports[4]);
  //cy.screenshot('Pass-test-004')
});

it('TC_012 Launch Few Report & see there is no error',()=>{
  logToGlobalFile('TC_012 Launch Few Report & see there is no error')
    dashboard.openMenu()
    setting.openMyReports()
    dashboard.openMenu()
    setting.openReportFromMyReport(data.report.reportname)
    cy.screenshot('pass-test-012_1')

    cy.wait(2000)
    dashboard.openMenu()
    setting.navigateReportType()
    setting.openReportFromReportTypes(data.reportType.ConnectorType,data.reportType.ReportName)
    cy.screenshot('pass-test-012_2')
    
    //dashboard.openMenu()
    //setting.navigatecategories()
    //dashboard.openMenu()
    //cy.wait(4000)
    //setting.openReportFromCatPage(data.category.category, data.category.subCategory1,  data.category.reportName)
    //cy.screenshot('pass-test-012_3')
})

it('TC_013 Mark some reports as favorites and see if they show up on dashboard',()=>{
  logToGlobalFile('TC_013 Mark some reports as favorites and see if they show up on dashboard')
  dashboard.openMenu()
  setting.openMyReports()
  dashboard.openMenu()
  setting.openReportFromMyReport(data.report.reportname)
  report.toggleReportFavoritesOn('0')
  dashboard.verifyFavoriteInDashboard(data.report.reportname)
  cy.screenshot('pass-test-013_1')
  cy.wait(5000)
  //gobal.searchAndSelectFirstReport(data.reportType.ReportName)
  
 
})

it('TC-014 Verify Creating a WF and share the WF with some user and see if the email is sent out', () => {   
  logToGlobalFile('TC-014_Verify Creating a WF and share the WF with some user and see if the email is sent out')
  gobal.searchAndSelectFirstReport(data.workflow.searchreport)
  const createWorkflow_userCount = createWorkflow_multipleuser(data.workflow.createWF, data.workflow.user)
  report.share_wf(data.workflow.createWF)
  report.verify_sharecount(data.workflow.createWF,createWorkflow_userCount)
  cy.screenshot('pass-test-014')
});


it('TC-036 Verify Thumbnail attributes functionality - disabling thumbnail attribute and verifying if DB is getting updated correctly, only enabled thumbnail attributes should display in "I" button of thumbnails across application', () => {
  logToGlobalFile('TC-036 Verify the thumbnail attributes across different widgets in the Dashboard');
  report.openDashReport()
  report.toggleReportsFavoritesOn()
  report.reportAbout()
  report.reportdetails()
  dashboard.clickHomeIcon()
  report.openDashReportinfo()
  report.openDashboardinfo_details()
  cy.wait(5000)
  //cy.wait(2000)
  cy.screenshot('screenshot-test-036_1')
  dashboard.openMenu()
  setting.openApplicationSettings()
  setting.clickcustomAttribute_off()
  dashboard.clickHomeIcon()
  report.openDashboardinfo_details()
  cy.wait(5000)
  cy.screenshot('screenshot-test-036_2')
  dashboard.openMenu()
  setting.openApplicationSettings()
  setting.clickcustomAttribute_on()
  dashboard.clickHomeIcon()
  report.openDashboardinfo_details()
  cy.wait(5000)
  cy.screenshot('screenshot-test-036_3')
  //report.openDashboardinfo_details_off()


  

})
it('TC-034 Switching between classic and modern dashboard', () => {
 logToGlobalFile('TC-034 Switching between classic and modern dashboard')
  dashboard.clickProfile()
  cy.screenshot('pass-test-034')
  dashboard.dashboardSetting()
  logToGlobalFile('Toggle dashboard style');
  dashboard.toggleDashboardStyle()
  //cy.screenshot('pass-test-034')
 

});




after(() => {
   
    logToGlobalFile();
  
  
});


})
